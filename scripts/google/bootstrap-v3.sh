#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# This script is passed as a startup-script to GCP instances
###################################################
###    NOTE: It is being run on _every_ boot    ###
###  It MUST be non destructive and itempotent  ###
###################################################

exec &> >(tee -a "/tmp/bootstrap.log")
set -x
env

# Pass env variables
for i in $(curl -s "http://metadata.google.internal/computeMetadata/v1/instance/attributes/" -H "Metadata-Flavor: Google"); do
    if [[ $i == CHEF* ]]; then
        export "$i"="$(curl -s "http://metadata.google.internal/computeMetadata/v1/instance/attributes/$i" -H "Metadata-Flavor: Google")"
    fi
    if [[ $i == GL* ]]; then
        export "$i"="$(curl -s "http://metadata.google.internal/computeMetadata/v1/instance/attributes/$i" -H "Metadata-Flavor: Google")"
    fi
done

# Lookup consul's service endpoint
apt-get install jq -y -q

# default to false, force a reformat even if there is an existing
# Linux filesystem
GL_FORMAT_DATA_DISK=${GL_FORMAT_DATA_DISK:-false}

if [[ -b /dev/sdb && ("true" ==  "${GL_FORMAT_DATA_DISK}" || $(file -sL /dev/sdb) != *Linux*) ]]; then
    mkfs.ext4 -m 0 -F -E lazy_itable_init=0,lazy_journal_init=0,discard /dev/sdb
fi

# Proceed with mounting
if [[ -L /dev/disk/by-id/google-persistent-disk-1 ]]; then
    disk_path="${GL_PERSISTENT_DISK_PATH:-/var/opt/gitlab}"
    mkdir -p "$disk_path"
    if ! grep -qs "$disk_path" /proc/mounts; then
        mount -o discard,defaults /dev/sdb "$disk_path"
    fi
    UUID="$(sudo blkid -s UUID -o value /dev/sdb)"
    if ! grep -qs "$UUID" /etc/fstab; then
        echo UUID="$UUID" "${GL_PERSISTENT_DISK_PATH:-/var/opt/gitlab}" ext4 discard,defaults 0 2 | tee -a /etc/fstab
    fi
fi

# Install chef

curl -L https://omnitruck.chef.io/install.sh | sudo bash -s -- -v "${CHEF_VERSION}"
mkdir /etc/chef

# Get validation.pem from gkms
gsutil cp gs://gitlab-gprd-chef-boostrap/validation.enc /tmp/validation.enc

gcloud kms decrypt --keyring=gitlab-gprd-bootstrap --location=global --key=gitlab-gprd-bootstrap-validation --plaintext-file=/etc/chef/validation.pem  --ciphertext-file=/tmp/validation.enc


rm -f /tmp/validator.enc

# create client.rb
cat > /etc/chef/client.rb <<-EOF
chef_server_url  "$CHEF_URL"
validation_client_name "gitlab-validator"
log_location   STDOUT
node_name "$CHEF_NODE_NAME"
environment "$CHEF_ENVIRONMENT"
EOF

# create run_list
cat > /etc/chef/first-run.json <<-EOF
{
  "run_list":[ $CHEF_RUN_LIST ]
}
EOF

# run chef
chef-client -j /etc/chef/first-run.json
