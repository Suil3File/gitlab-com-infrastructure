#!/bin/bash
CHEF_NODE_NAME="$(curl -s "http://metadata.google.internal/computeMetadata/v1/instance/attributes/CHEF_NODE_NAME" -H "Metadata-Flavor: Google")"
if [[ ! -f /etc/chef/client.pem ]]; then
    # No client.pem, nothing to do
    exit 0
fi

if type -P knife >/dev/null; then
    knife node delete "$CHEF_NODE_NAME" -c /etc/chef/client.rb -y
    knife client delete "$CHEF_NODE_NAME" -c /etc/chef/client.rb -y
fi

rm -f /etc/chef/client.pem
