## State storage
terraform {
  backend "s3" {}
}

## AWS
provider "aws" {
  region = "us-east-1"
}

## Google

provider "google" {
  project = "${var.project}"
  region  = "${var.region}"
  version = "~> 1.16.2"
}

/*
##################################
#
#  NAT gateway
#
#################################
module "nat" {
  source     = "GoogleCloudPlatform/nat-gateway/google"
  region     = "${var.region}"
  network    = "${var.environment}"
}
*/
##################################
#
#  Network
#
#################################

module "network" {
  environment = "${var.environment}"
  project     = "${var.project}"
  source      = "../../modules/google/vpc"
}

##################################
#
#  Network Peering
#
#################################

resource "google_compute_network_peering" "peering_ops" {
  name         = "peering-ops"
  network      = "${var.network_env}"
  peer_network = "${var.network_ops}"
}

##################################
#
#  Web front-end
#
#################################

module "web" {
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-fe-web]\""
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  ip_cidr_range         = "${var.subnetworks["web"]}"
  kernel_version        = "${var.default_kernel_version}"
  machine_type          = "${var.machine_types["web"]}"
  name                  = "web"
  node_count            = "${var.node_count["web"]}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["web"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 443
  source                = "../../modules/google/generic-sv-with-group"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  API
#
#################################

module "api" {
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-fe-api]\""
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  ip_cidr_range         = "${var.subnetworks["api"]}"
  kernel_version        = "${var.default_kernel_version}"
  machine_type          = "${var.machine_types["api"]}"
  name                  = "api"
  node_count            = "${var.node_count["api"]}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["api"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 443
  source                = "../../modules/google/generic-sv-with-group"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  Git
#
##################################

module "git" {
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-fe-git]\""
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  ip_cidr_range         = "${var.subnetworks["git"]}"
  kernel_version        = "${var.default_kernel_version}"
  machine_type          = "${var.machine_types["git"]}"
  name                  = "git"
  node_count            = "${var.node_count["git"]}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["git"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 22
  source                = "../../modules/google/generic-sv-with-group"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  registry front-end
#
#################################

module "registry" {
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-fe-registry]\""
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  ip_cidr_range         = "${var.subnetworks["registry"]}"
  kernel_version        = "${var.default_kernel_version}"
  machine_type          = "${var.machine_types["registry"]}"
  name                  = "registry"
  node_count            = "${var.node_count["registry"]}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["registry"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 22
  source                = "../../modules/google/generic-sv-with-group"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  Database
#
#################################

module "postgres" {
  bootstrap_version     = 6
  chef_init_run_list    = "\"recipe[gitlab-server::hack_gitlab_ctl_reconfigure]\""
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-db-postgres]\",\"role[${var.environment}-base-db-postgres-replication]\""
  data_disk_size        = 5000
  data_disk_type        = "pd-ssd"
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  ip_cidr_range         = "${var.subnetworks["db"]}"
  kernel_version        = "${var.default_kernel_version}"
  machine_type          = "${var.machine_types["db"]}"
  name                  = "postgres"
  node_count            = "${var.node_count["db"]}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["db"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  source                = "../../modules/google/generic-stor"
  tier                  = "db"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

#############################################
#
#  GCP Internal TCP LoadBalancer and PgBouncer
#
#############################################

module "gcp-tcp-lb-internal-pgbouncer" {
  backend_service        = "${module.pg-bouncer.google_compute_region_backend_service_self_link}"
  environment            = "${var.environment}"
  external               = false
  forwarding_port_ranges = ["6432"]
  fqdns                  = "${var.lb_fqdns_internal_pgbouncer}"
  gitlab_zone_id         = "${var.gitlab_net_zone_id}"
  health_check_ports     = ["6432"]
  instances              = ["${module.pg-bouncer.instances_self_link}"]
  lb_count               = "1"
  name                   = "gcp-tcp-lb-internal-pgbouncer"
  names                  = ["${var.environment}-pgbouncer"]
  project                = "${var.project}"
  region                 = "${var.region}"
  source                 = "../../modules/google/tcp-lb"
  subnetwork_self_link   = "${module.pg-bouncer.google_compute_subnetwork_self_link}"
  targets                = ["pgbouncer"]
  vpc                    = "${module.network.self_link}"
}

module "pg-bouncer" {
  backend_service_type   = "regional"
  bootstrap_version      = 6
  chef_init_run_list     = "\"recipe[gitlab-server::hack_gitlab_ctl_reconfigure]\""
  chef_provision         = "${var.chef_provision}"
  chef_run_list          = "\"role[${var.environment}-base-db-pgbouncer]\""
  create_backend_service = true
  dns_zone_name          = "${var.dns_zone_name}"
  environment            = "${var.environment}"
  health_check           = "tcp"
  ip_cidr_range          = "${var.subnetworks["pgb"]}"
  kernel_version         = "${var.default_kernel_version}"
  machine_type           = "${var.machine_types["pgb"]}"
  name                   = "pgbouncer"
  node_count             = "${var.node_count["pgb"]}"
  project                = "${var.project}"
  public_ports           = "${var.public_ports["pgb"]}"
  region                 = "${var.region}"
  service_account_email  = "${var.service_account_email}"
  service_port           = 6432
  source                 = "../../modules/google/generic-sv-with-group"
  tier                   = "db"
  use_new_node_name      = true
  vpc                    = "${module.network.self_link}"
}

#############################################
module "geo-postgres" {
  bootstrap_version     = 6
  chef_init_run_list    = "\"recipe[gitlab-server::hack_gitlab_ctl_reconfigure]\""
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-db-geo-postgres]\""
  data_disk_size        = 5000
  data_disk_type        = "pd-ssd"
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  ip_cidr_range         = "${var.subnetworks["geodb"]}"
  kernel_version        = "${var.default_kernel_version}"
  machine_type          = "${var.machine_types["geodb"]}"
  name                  = "geo-postgres"
  node_count            = "${var.node_count["geodb"]}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["geodb"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  source                = "../../modules/google/generic-stor"
  tier                  = "db"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  Redis
#
##################################

module "redis" {
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-db-redis-server-single]\""
  data_disk_size        = 100
  data_disk_type        = "pd-ssd"
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  ip_cidr_range         = "${var.subnetworks["redis"]}"
  kernel_version        = "${var.default_kernel_version}"
  machine_type          = "${var.machine_types["redis"]}"
  name                  = "redis"
  node_count            = "${var.node_count["redis"]}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["redis"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  source                = "../../modules/google/generic-stor"
  tier                  = "db"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

module "redis-cache" {
  bootstrap_version       = 6
  chef_provision          = "${var.chef_provision}"
  dns_zone_name           = "${var.dns_zone_name}"
  environment             = "${var.environment}"
  ip_cidr_range           = "${var.subnetworks["redis-cache"]}"
  kernel_version          = "${var.default_kernel_version}"
  name                    = "redis-cache"
  project                 = "${var.project}"
  public_ports            = "${var.public_ports["redis-cache"]}"
  redis_chef_run_list     = "\"role[${var.environment}-base-db-redis-server-cache]\""
  redis_count             = "${var.node_count["redis-cache"]}"
  redis_data_disk_size    = 100
  redis_data_disk_type    = "pd-ssd"
  redis_machine_type      = "${var.machine_types["redis-cache"]}"
  region                  = "${var.region}"
  sentinel_chef_run_list  = "\"role[${var.environment}-base-db-redis-sentinel-cache]\""
  sentinel_count          = "${var.node_count["redis-cache-sentinel"]}"
  sentinel_data_disk_size = 100
  sentinel_data_disk_type = "pd-ssd"
  sentinel_machine_type   = "${var.machine_types["redis-cache-sentinel"]}"
  service_account_email   = "${var.service_account_email}"
  source                  = "../../modules/google/generic-stor-redis"
  tier                    = "db"
  use_new_node_name       = true
  vpc                     = "${module.network.self_link}"
}

##################################
#
#  Sidekiq
#
##################################

module "sidekiq" {
  bootstrap_version                   = 6
  chef_provision                      = "${var.chef_provision}"
  chef_run_list                       = "\"role[${var.environment}-base-be-sidekiq-besteffort]\""
  dns_zone_name                       = "${var.dns_zone_name}"
  environment                         = "${var.environment}"
  ip_cidr_range                       = "${var.subnetworks["sidekiq"]}"
  kernel_version                      = "${var.default_kernel_version}"
  machine_type                        = "${var.machine_types["sidekiq-besteffort"]}"
  name                                = "sidekiq"
  project                             = "${var.project}"
  public_ports                        = "${var.public_ports["sidekiq"]}"
  region                              = "${var.region}"
  service_account_email               = "${var.service_account_email}"
  sidekiq_asap_count                  = "${var.node_count["sidekiq-asap"]}"
  sidekiq_asap_instance_type          = "${var.machine_types["sidekiq-asap"]}"
  sidekiq_besteffort_count            = "${var.node_count["sidekiq-besteffort"]}"
  sidekiq_besteffort_instance_type    = "${var.machine_types["sidekiq-besteffort"]}"
  sidekiq_elasticsearch_count         = "${var.node_count["sidekiq-elasticsearch"]}"
  sidekiq_elasticsearch_instance_type = "${var.machine_types["sidekiq-elasticsearch"]}"
  sidekiq_pages_count                 = "${var.node_count["sidekiq-pages"]}"
  sidekiq_pages_instance_type         = "${var.machine_types["sidekiq-pages"]}"
  sidekiq_pipeline_count              = "${var.node_count["sidekiq-pipeline"]}"
  sidekiq_pipeline_instance_type      = "${var.machine_types["sidekiq-pipeline"]}"
  sidekiq_pullmirror_count            = "${var.node_count["sidekiq-pullmirror"]}"
  sidekiq_pullmirror_instance_type    = "${var.machine_types["sidekiq-pullmirror"]}"
  sidekiq_realtime_count              = "${var.node_count["sidekiq-realtime"]}"
  sidekiq_realtime_instance_type      = "${var.machine_types["sidekiq-realtime"]}"
  sidekiq_traces_count                = "${var.node_count["sidekiq-traces"]}"
  sidekiq_traces_instance_type        = "${var.machine_types["sidekiq-traces"]}"
  source                              = "../../modules/google/generic-sv-sidekiq"
  tier                                = "sv"
  use_new_node_name                   = true
  vpc                                 = "${module.network.self_link}"
}

##################################
#
#  Mailroom
#
##################################

module "mailroom" {
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-be-mailroom]\""
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  ip_cidr_range         = "${var.subnetworks["mailroom"]}"
  kernel_version        = "${var.default_kernel_version}"
  machine_type          = "${var.machine_types["mailroom"]}"
  name                  = "mailroom"
  node_count            = "${var.node_count["mailroom"]}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["mailroom"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 22
  source                = "../../modules/google/generic-sv-with-group"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  Storage nodes for repositories
#
##################################

module "file" {
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-stor-nfs]\""
  data_disk_size        = "${var.data_disk_sizes["file"]}"
  data_disk_type        = "pd-ssd"
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  ip_cidr_range         = "${var.subnetworks["stor"]}"
  kernel_version        = "${var.default_kernel_version}"
  machine_type          = "${var.machine_types["stor"]}"
  name                  = "file"
  node_count            = "${var.node_count["stor"]}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["stor"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  source                = "../../modules/google/generic-stor"
  tier                  = "stor"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
  zone                  = "us-east1-c"
}

##################################
#
#  Storage nodes for
#  uploads/lfs/pages/artifacts/builds/cache
#
#  share:
#    gitlab-ci/builds
#    gitlab-rails/shared/cache
#    gitlab-rails/shared/tmp
#    gitlab-rails/uploads
#    gitlab-rails/shared/lfs-objects
#    gitlab-rails/shared/artifacts
#
#  pages:
#    gitlab-rails/shared/pages
#
##################################

module "share" {
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-stor]\""
  data_disk_size        = "${var.data_disk_sizes["share"]}"
  data_disk_type        = "pd-standard"
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  ip_cidr_range         = "${var.subnetworks["share"]}"
  kernel_version        = "${var.default_kernel_version}"
  machine_type          = "${var.machine_types["stor"]}"
  name                  = "share"
  node_count            = "${var.node_count["share"]}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["stor"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  source                = "../../modules/google/generic-stor"
  tier                  = "stor"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

## Pages has a DNS entry for failover rsync
## This and leaving port 22 open can be removed
## after failover.

resource "aws_route53_record" "pages" {
  zone_id = "${var.gitlab_net_zone_id}"
  name    = "pages.stor.${var.environment}.gitlab.net"
  type    = "A"
  ttl     = "300"
  records = ["${module.pages.instance_public_ips}"]
}

module "pages" {
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-stor]\""
  data_disk_size        = "${var.data_disk_sizes["pages"]}"
  data_disk_type        = "pd-standard"
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  ip_cidr_range         = "${var.subnetworks["pages"]}"
  kernel_version        = "${var.default_kernel_version}"
  machine_type          = "${var.machine_types["stor"]}"
  name                  = "pages"
  node_count            = "${var.node_count["pages"]}"
  project               = "${var.project}"
  public_ports          = "${concat(var.public_ports["stor"], list("22"))}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  source                = "../../modules/google/generic-stor"
  tier                  = "stor"
  use_new_node_name     = true
  use_external_ip       = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  External HAProxy LoadBalancer
#
##################################

module "fe-lb" {
  bootstrap_version      = 6
  chef_provision         = "${var.chef_provision}"
  chef_run_list          = "\"role[${var.environment}-base-lb-fe]\""
  create_backend_service = false
  dns_zone_name          = "${var.dns_zone_name}"
  environment            = "${var.environment}"
  health_check           = "http"
  ip_cidr_range          = "${var.subnetworks["fe-lb"]}"
  kernel_version         = "${var.default_kernel_version}"
  machine_type           = "${var.machine_types["fe-lb"]}"
  name                   = "fe"
  node_count             = "${var.node_count["fe-lb"]}"
  project                = "${var.project}"
  public_ports           = "${var.public_ports["fe-lb"]}"
  region                 = "${var.region}"
  service_account_email  = "${var.service_account_email}"
  service_path           = "/-/available-https"
  service_port           = 8002
  source                 = "../../modules/google/generic-sv-with-group"
  tier                   = "lb"
  use_new_node_name      = true
  vpc                    = "${module.network.self_link}"
}

##################################
#
#  External HAProxy LoadBalancer Pages
#
##################################

module "fe-lb-pages" {
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-lb-pages]\""
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "http"
  ip_cidr_range         = "${var.subnetworks["fe-lb-pages"]}"
  kernel_version        = "${var.default_kernel_version}"
  machine_type          = "${var.machine_types["fe-lb"]}"
  name                  = "fe-pages"
  node_count            = "${var.node_count["fe-lb-pages"]}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["fe-lb"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 7331
  source                = "../../modules/google/generic-sv-with-group"
  tier                  = "lb"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  External HAProxy LoadBalancer AltSSH
#
##################################

module "fe-lb-altssh" {
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-lb-altssh]\""
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "http"
  ip_cidr_range         = "${var.subnetworks["fe-lb-altssh"]}"
  kernel_version        = "${var.default_kernel_version}"
  machine_type          = "${var.machine_types["fe-lb"]}"
  name                  = "fe-altssh"
  node_count            = "${var.node_count["fe-lb-altssh"]}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["fe-lb"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 7331
  source                = "../../modules/google/generic-sv-with-group"
  tier                  = "lb"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  GCP TCP LoadBalancers
#
##################################

#### Load balancer for the main site
module "gcp-tcp-lb" {
  environment            = "${var.environment}"
  forwarding_port_ranges = "${var.tcp_lbs["forwarding_port_ranges"]}"
  fqdns                  = "${var.lb_fqdns}"
  gitlab_zone_id         = "${var.gitlab_com_zone_id}"
  health_check_ports     = "${var.tcp_lbs["health_check_ports"]}"
  instances              = ["${module.fe-lb.instances_self_link}"]
  lb_count               = "${length(var.tcp_lbs["names"])}"
  name                   = "gcp-tcp-lb"
  names                  = "${var.tcp_lbs["names"]}"
  project                = "${var.project}"
  region                 = "${var.region}"
  source                 = "../../modules/google/tcp-lb"
  targets                = ["fe"]
}

##################################
#
#  GCP Internal TCP LoadBalancers
#
##################################
### The regional backend service that is required for the internal
### load balancer. Unlike global backend services every instance
### group _must_ contain at least one instance. Also you cannot
### have both a global and a regional backend service.

resource "google_compute_region_backend_service" "internal-lb" {
  backend {
    group = "${module.fe-lb.instance_groups_self_link[1]}"
  }

  health_checks = ["${module.fe-lb.http_health_check_self_link}"]
  name          = "${format("%v-internal-lb", var.environment)}"
  protocol      = "TCP"
}

###### Internal Load balancer for the main site
module "gcp-tcp-lb-internal" {
  backend_service        = "${google_compute_region_backend_service.internal-lb.self_link}"
  environment            = "${var.environment}"
  external               = false
  forwarding_port_ranges = "${var.tcp_lbs_internal["forwarding_port_ranges"]}"
  fqdns                  = "${var.lb_fqdns_internal}"
  gitlab_zone_id         = "${var.gitlab_net_zone_id}"
  health_check_ports     = "${var.tcp_lbs_internal["health_check_ports"]}"
  instances              = ["${module.fe-lb.instances_self_link}"]
  lb_count               = "${length(var.tcp_lbs_internal["names"])}"
  name                   = "gcp-tcp-lb-internal"
  names                  = "${var.tcp_lbs_internal["names"]}"
  project                = "${var.project}"
  region                 = "${var.region}"
  source                 = "../../modules/google/tcp-lb"
  subnetwork_self_link   = "${module.fe-lb.google_compute_subnetwork_self_link}"
  targets                = ["fe"]
  vpc                    = "${module.network.self_link}"
}

#### Load balancer for pages
module "gcp-tcp-lb-pages" {
  environment            = "${var.environment}"
  forwarding_port_ranges = "${var.tcp_lbs_pages["forwarding_port_ranges"]}"
  fqdns                  = "${var.lb_fqdns_pages}"
  gitlab_zone_id         = "${var.gitlab_io_zone_id}"
  health_check_ports     = "${var.tcp_lbs_pages["health_check_ports"]}"
  instances              = ["${module.fe-lb-pages.instances_self_link}"]
  lb_count               = "${length(var.tcp_lbs_pages["names"])}"
  name                   = "gcp-tcp-lb-pages"
  names                  = "${var.tcp_lbs_pages["names"]}"
  project                = "${var.project}"
  region                 = "${var.region}"
  source                 = "../../modules/google/tcp-lb"
  targets                = ["fe-pages"]
}

#### Load balancer for altssh
module "gcp-tcp-lb-altssh" {
  environment                = "${var.environment}"
  forwarding_port_ranges     = "${var.tcp_lbs_altssh["forwarding_port_ranges"]}"
  fqdns                      = "${var.lb_fqdns_altssh}"
  gitlab_zone_id             = "${var.gitlab_com_zone_id}"
  health_check_ports         = "${var.tcp_lbs_altssh["health_check_ports"]}"
  health_check_request_paths = "${var.tcp_lbs_altssh["health_check_request_paths"]}"
  instances                  = ["${module.fe-lb-altssh.instances_self_link}"]
  lb_count                   = "${length(var.tcp_lbs_altssh["names"])}"
  name                       = "gcp-tcp-lb-altssh"
  names                      = "${var.tcp_lbs_altssh["names"]}"
  project                    = "${var.project}"
  region                     = "${var.region}"
  source                     = "../../modules/google/tcp-lb"
  targets                    = ["fe-altssh"]
}

#### Load balancer for bastion
module "gcp-tcp-lb-bastion" {
  environment            = "${var.environment}"
  forwarding_port_ranges = "${var.tcp_lbs_bastion["forwarding_port_ranges"]}"
  fqdns                  = "${var.lb_fqdns_bastion}"
  gitlab_zone_id         = "${var.gitlab_com_zone_id}"
  health_check_ports     = "${var.tcp_lbs_bastion["health_check_ports"]}"
  instances              = ["${module.bastion.instances_self_link}"]
  lb_count               = "${length(var.tcp_lbs_bastion["names"])}"
  name                   = "gcp-tcp-lb-bastion"
  names                  = "${var.tcp_lbs_bastion["names"]}"
  project                = "${var.project}"
  region                 = "${var.region}"
  session_affinity       = "CLIENT_IP"
  source                 = "../../modules/google/tcp-lb"
  targets                = ["bastion"]
}

##################################
#
#  Consul
#
##################################

module "consul" {
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-infra-consul]\""
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  ip_cidr_range         = "${var.subnetworks["consul"]}"
  kernel_version        = "${var.default_kernel_version}"
  machine_type          = "${var.machine_types["consul"]}"
  name                  = "consul"
  node_count            = "${var.node_count["consul"]}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["consul"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 8300
  source                = "../../modules/google/generic-sv-with-group"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  Pubsubbeats
#
#  Machines for running the beats
#  that consume logs from pubsub
#  and send them to elastic cloud
#
#  You must have a chef role with the
#  following format:
#     role[<env>-infra-pubsubbeat-<beat_name>]
#
##################################

module "pubsubbeat" {
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  ip_cidr_range         = "${var.subnetworks["pubsubbeat"]}"
  kernel_version        = "${var.default_kernel_version}"
  machine_types         = "${var.pubsubbeats["machine_types"]}"
  names                 = "${var.pubsubbeats["names"]}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["pubsubbeat"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 22
  source                = "../../modules/google/pubsubbeat"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  Monitoring
#
#  Uses the monitoring module, this
#  creates a single instance behind
#  a load balancer with identity aware
#  proxy enabled.
#
##################################

resource "google_compute_subnetwork" "monitoring" {
  ip_cidr_range            = "${var.subnetworks["monitoring"]}"
  name                     = "${format("monitoring-%v", var.environment)}"
  network                  = "${module.network.self_link}"
  private_ip_google_access = true
  project                  = "${var.project}"
  region                   = "${var.region}"
}

#######################
#
# load balancer for all hosts in this section
#
#######################

module "monitoring-lb" {
  cert_link          = "${var.monitoring_cert_link}"
  environment        = "${var.environment}"
  gitlab_net_zone_id = "${var.gitlab_net_zone_id}"
  hosts              = ["${var.monitoring_hosts["names"]}"]
  name               = "monitoring-lb"
  project            = "${var.project}"
  region             = "${var.region}"
  service_ports      = ["${var.monitoring_hosts["ports"]}"]
  source             = "../../modules/google/monitoring-lb"
  subnetwork_name    = "${google_compute_subnetwork.monitoring.name}"
  targets            = ["${var.monitoring_hosts["names"]}"]
  url_map            = "${google_compute_url_map.monitoring-lb.self_link}"
}

#######################
module "prometheus" {
  attach_data_disk      = true
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-infra-prometheus]\""
  data_disk_size        = 1000
  data_disk_type        = "pd-standard"
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  kernel_version        = "${var.default_kernel_version}"
  machine_type          = "${var.machine_types["monitoring"]}"
  name                  = "prometheus"
  node_count            = "${var.node_count["prometheus"]}"
  oauth2_client_id      = "${var.oauth2_client_id_monitoring}"
  oauth2_client_secret  = "${var.oauth2_client_secret_monitoring}"
  persistent_disk_path  = "/opt/prometheus"
  project               = "${var.project}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_path          = "/graph"
  service_port          = "${element(var.monitoring_hosts["ports"], index(var.monitoring_hosts["names"], "prometheus"))}"
  source                = "../../modules/google/monitoring-with-count"
  subnetwork_name       = "${google_compute_subnetwork.monitoring.name}"
  tier                  = "inf"
  use_new_node_name     = true
}

module "prometheus-app" {
  attach_data_disk      = true
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-infra-prometheus-app]\""
  data_disk_size        = 1000
  data_disk_type        = "pd-standard"
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  kernel_version        = "${var.default_kernel_version}"
  machine_type          = "${var.machine_types["monitoring"]}"
  name                  = "prometheus-app"
  node_count            = "${var.node_count["prometheus-app"]}"
  oauth2_client_id      = "${var.oauth2_client_id_monitoring}"
  oauth2_client_secret  = "${var.oauth2_client_secret_monitoring}"
  persistent_disk_path  = "/opt/prometheus"
  project               = "${var.project}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_path          = "/graph"
  service_port          = "${element(var.monitoring_hosts["ports"], index(var.monitoring_hosts["names"], "prometheus-app"))}"
  source                = "../../modules/google/monitoring-with-count"
  subnetwork_name       = "${google_compute_subnetwork.monitoring.name}"
  tier                  = "inf"
  use_new_node_name     = true
}

module "alerts" {
  attach_data_disk      = true
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-infra-alerts]\""
  data_disk_size        = 100
  data_disk_type        = "pd-standard"
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  kernel_version        = "${var.default_kernel_version}"
  machine_type          = "${var.machine_types["monitoring"]}"
  name                  = "alerts"
  node_count            = "${var.node_count["alerts"]}"
  oauth2_client_id      = "${var.oauth2_client_id_monitoring}"
  oauth2_client_secret  = "${var.oauth2_client_secret_monitoring}"
  persistent_disk_path  = "/opt"
  project               = "${var.project}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = "${element(var.monitoring_hosts["ports"], index(var.monitoring_hosts["names"], "alerts"))}"
  source                = "../../modules/google/monitoring-with-count"
  subnetwork_name       = "${google_compute_subnetwork.monitoring.name}"
  tier                  = "inf"
  use_new_node_name     = true
}

module "sd-exporter" {
  additional_scopes         = ["https://www.googleapis.com/auth/monitoring"]
  allow_stopping_for_update = true
  bootstrap_version         = 6
  chef_provision            = "${var.chef_provision}"
  chef_run_list             = "\"role[${var.environment}-infra-sd-exporter]\""
  create_backend_service    = false
  dns_zone_name             = "${var.dns_zone_name}"
  environment               = "${var.environment}"
  kernel_version            = "${var.default_kernel_version}"
  machine_type              = "${var.machine_types["sd-exporter"]}"
  name                      = "sd-exporter"
  node_count                = "${var.node_count["sd-exporter"]}"
  project                   = "${var.project}"
  public_ports              = "${var.public_ports["sd-exporter"]}"
  region                    = "${var.region}"
  service_account_email     = "${var.service_account_email}"
  source                    = "../../modules/google/generic-sv-with-group"
  subnetwork_name           = "${google_compute_subnetwork.monitoring.name}"
  tier                      = "inf"
  use_new_node_name         = true
  vpc                       = "${module.network.self_link}"
}

module "blackbox" {
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-blackbox]\""
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  kernel_version        = "${var.default_kernel_version}"
  machine_type          = "${var.machine_types["blackbox"]}"
  name                  = "blackbox"
  node_count            = "${var.node_count["blackbox"]}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["blackbox"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 22
  source                = "../../modules/google/generic-sv-with-group"
  subnetwork_name       = "${google_compute_subnetwork.monitoring.name}"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
  use_external_ip       = true
}

module "influxdb" {
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-influxdb]\""
  data_disk_size        = 1000
  data_disk_type        = "pd-ssd"
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  ip_cidr_range         = "${var.subnetworks["influxdb"]}"
  kernel_version        = "${var.default_kernel_version}"
  machine_type          = "${var.machine_types["influxdb"]}"
  name                  = "influxdb"
  node_count            = "${var.node_count["influxdb"]}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["influxdb"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  source                = "../../modules/google/generic-stor"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  Console
#
##################################

module "console" {
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-console-node]\""
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  ip_cidr_range         = "${var.subnetworks["console"]}"
  kernel_version        = "${var.default_kernel_version}"
  machine_type          = "${var.machine_types["console"]}"
  name                  = "console"
  node_count            = "${var.node_count["console"]}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["console"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 22
  source                = "../../modules/google/generic-sv-with-group"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  Deploy
#
##################################

module "deploy" {
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-deploy-node]\""
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  ip_cidr_range         = "${var.subnetworks["deploy"]}"
  kernel_version        = "${var.default_kernel_version}"
  machine_type          = "${var.machine_types["deploy"]}"
  name                  = "deploy"
  node_count            = "${var.node_count["deploy"]}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["deploy"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 22
  source                = "../../modules/google/generic-sv-with-group"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  Runner
#
##################################

module "runner" {
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-runner]\""
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  ip_cidr_range         = "${var.subnetworks["runner"]}"
  kernel_version        = "${var.default_kernel_version}"
  machine_type          = "${var.machine_types["runner"]}"
  name                  = "runner"
  node_count            = "${var.node_count["runner"]}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["runner"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 22
  source                = "../../modules/google/generic-sv-with-group"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  VPN connection to Azure
#
#  Currently disabled as it isn't
#  necessary
#
##################################

module "google-azure-vpn" {
  dest_subnet   = "${var.vpn_dest_subnet}"
  name          = "gcp-azure-${var.environment}"
  network_link  = "${module.network.name}"
  network_name  = "${module.network.self_link}"
  peer_ip       = "${var.vpn_peer_address}"
  region        = "${var.region}"
  shared_secret = "${var.vpn_shared_secret}"
  source        = "../../modules/google/vpn"
  source_subnet = "${var.vpn_source_subnet}"
}

##################################
#
#  Bastion
#
##################################

module "bastion" {
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-bastion]\""
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "http"
  ip_cidr_range         = "${var.subnetworks["bastion"]}"
  kernel_version        = "${var.default_kernel_version}"
  machine_type          = "${var.machine_types["bastion"]}"
  name                  = "bastion"
  node_count            = "${var.node_count["bastion"]}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["bastion"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 22
  source                = "../../modules/google/generic-sv-with-group"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  Object storage buckets
#
##################################

module "gitlab_object_storage" {
  environment                       = "${var.environment}"
  versioning                        = "${var.versioning}"
  artifact_age                      = "${var.artifact_age}"
  lfs_object_age                    = "${var.lfs_object_age}"
  upload_age                        = "${var.upload_age}"
  storage_log_age                   = "${var.storage_log_age}"
  storage_class                     = "${var.storage_class}"
  gcs_service_account_email         = "${var.gcs_service_account_email}"
  gcs_storage_analytics_group_email = "${var.gcs_storage_analytics_group_email}"
  source                            = "../../modules/google/storage-buckets"
}
