# GitLab.com Infrastructure

## What is this?

Here you can find the Terraform configuration for GitLab.com, the staging environment and possibly something else.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written. Reading [Terraform: Up and Running](https://www.amazon.com/Terraform-Running-Writing-Infrastructure-Code-ebook/dp/B06XKHGJHP) is also recommended.

## How the repository is structured

Each environment has its own directory.

There is currently one [Terraform state](https://www.terraform.io/docs/state/index.html) for each environment although we're considering to split at least the production one into smaller ones.

Cattle is handled in loops while pets require the full configuration to be coded. This means you only need to change `count` in `main.tf` to scale a fleet in and out.

## Setup

There are two wrapper scripts in the `bin/` directory that should be used in place of the `terraform` executable, `tf` and `tf-init`. Add the `bin/` directory to your `$PATH`.

### Terraform Variables and Environment Variables

You are required to provide all the secrets through shell variables or Terraform variables.
Secrets for the different environments can be found in 1Password.

* [Terraform variables](https://www.terraform.io/docs/configuration/variables.html) are stored in `private/env_vars` in a file called `<environment>.tfvars`.

* Shell variables are stored in `private/env_vars` in a file called `<environment>.env`.  This file is sourced by the `bin/tf` and `bin/tf-init` wrapper scripts. All environment variables that are necessary for the environment should be defined here.

For example, production and staging:

```
./production.tfvars
./production.env
./staging.tfvars
./staging.env
etc...
```

## How can I use this

First and foremost, remember that Terraform is an extremely powerful tool so be sure you know how to use it well before applying changes in production. Always `plan` and don't hesitate to ask in `#production` if you are in doubt or you see something off in the plan, even the smallest thing.

### Important notes

* We use differing versions of terraform in our environments.  You'll want to
  ensure you have all available versions prior running anything in terraform.
  * `find ./ -name .terraform_version -exec cat {} \;`
  * our wrapper scripts mentioned above will help protect you from running
    terraform with the incorrect desired version
* Make sure you're not specifying `knife[:identity_file]` in your `.chef/knife.rb`. If you do need it then comment it out every time you create a new node. If you don't then the node provisioning will fail with an error like this: `Errno::ENODEV: Operation not supported by device`.
* Be extremely careful when you initialise the state for the first time. Do not load the variables on staging and then `cd` to production to initialise the state: this would link production to the staging state and you do not want that. It's good practice to initialise the states as soon as you set up your environment to avoid making mistakes, so:
```
cd staging
tf-init
cd ../production
tf-init
...
```
Then start working.

#### Getting Started:

  1. Clone [chef-repo](https://dev.gitlab.org/cookbooks/chef-repo) if you haven't already.
  1. Install `pwgen`. If you're using brew you can simply run `brew install pwgen`.
  1. It is highly suggested to utilize some sort of terraform versioning manager such as [tfenv](https://github.com/kamatama41/tfenv) with `brew install tfenv` in order to manage multiple versions of Terraform. So if _for instance_ you want to install version 0.9.8 all you have to do it run `tfenv install 0.9.8` and enable it with `tfenv use 0.9.8`. To verify this, run `terraform -version` and check the version string. See the _Important Notes_ section above to determine which versions are used in our environment.
  1. Ensure that gitlab-com-infrastructure/bin is in your path
  1. In 1password search for "terraform-private" and create each of the`*.env` files in the `private/env_vars` directory
  1. Be sure that the [gcloud-cli is configured and authenticated](https://gitlab.com/gitlab-com/runbooks/blob/master/howto/gcloud-cli.md)
  1. Set up access via [GSTG bastion hosts](https://gitlab.com/gitlab-com/runbooks/blob/master/howto/gstg-bastions.md)
  1. Run `tf-init` and `tf plan` in the `environments/gstg` directory and ensure that terraform runs and you can see pending changes

### Caveats

- Don't use `tf destroy` on cattle unless you **really** know what you're doing. Be aware that deleting an element of an array other than the last one will result in all the resources starting from that element to be destroyed as well. So if you delete `module.something.resource[0]` Terraform will delete **everything** on that array, even if the confirmation dialog only reports the one you want to delete.

---

## Contributing

See [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
