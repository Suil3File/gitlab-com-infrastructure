resource "azurerm_availability_set" "sentinel" {
  name                         = "${format("sentinel-%v-%v", var.persistence, var.environment)}"
  location                     = "${var.location}"
  managed                      = true
  platform_update_domain_count = 20
  platform_fault_domain_count  = 3
  resource_group_name          = "${var.resource_group_name}"
}

resource "azurerm_network_interface" "sentinel" {
  count                   = "${var.sentinel_count}"
  name                    = "${format("sentinel-%v-%02d-%v-%v", var.persistence, count.index + 1, var.tier, var.environment)}"
  internal_dns_name_label = "${format("sentinel-%v-%02d-%v-%v", var.persistence, count.index + 1, var.tier, var.environment)}"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "${format("sentinel-%v-%02d-%v", var.persistence, count.index + 1, var.environment)}"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "${join(".", slice(split(".", var.address_prefix), 0, 3))}.${count.index + var.ip_skew + 11}"
  }
}

resource "aws_route53_record" "sentinel" {
  count   = "${var.sentinel_count}"
  zone_id = "${var.gitlab_com_zone_id}"
  name    = "${format("sentinel-%v-%02d.%v.%v.gitlab.com.", var.persistence, count.index + 1, var.tier, var.environment == "prod" ? "prd" : var.environment)}"
  type    = "A"
  ttl     = "300"
  records = ["${azurerm_network_interface.sentinel.*.private_ip_address[count.index]}"]
}

data "template_file" "chef-bootstrap-sentinel" {
  count    = "${var.sentinel_count}"
  template = "${file("${path.root}/../../templates/chef-bootstrap-ssh-keys.tpl")}"

  vars {
    ip_address      = "${azurerm_network_interface.sentinel.*.private_ip_address[count.index]}"
    hostname        = "${format("sentinel-%v-%02d.%v.%v.gitlab.com", var.persistence, count.index + 1, var.tier, var.environment == "prod" ? "prd" : var.environment)}"
    chef_repo_dir   = "${var.chef_repo_dir}"
    ssh_user        = "${var.ssh_user}"
    ssh_private_key = "${var.ssh_private_key}"
    chef_vaults     = "${var.chef_vaults}"
    chef_vault_env  = "${var.chef_vault_env}"
    chef_version    = "${var.chef_version}"
    environment     = "${var.environment}"
  }
}

resource "azurerm_virtual_machine" "sentinel" {
  count                         = "${var.sentinel_count}"
  name                          = "${format("sentinel-%v-%02d.%v.%v.gitlab.com", var.persistence, count.index + 1, var.tier, var.environment == "prod" ? "prd" : var.environment)}"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  availability_set_id           = "${azurerm_availability_set.sentinel.id}"
  network_interface_ids         = ["${azurerm_network_interface.sentinel.*.id[count.index]}"]
  primary_network_interface_id  = "${azurerm_network_interface.sentinel.*.id[count.index]}"
  vm_size                       = "${var.sentinel_instance_type}"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "${format("osdisk-sentinel-%v-%02d-%v", var.persistence, count.index + 1, var.environment)}"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "${format("sentinel-%v-%02d.%v.%v.gitlab.com", var.persistence, count.index + 1, var.tier, var.environment == "prod" ? "prd" : var.environment)}"
    admin_username = "${var.ssh_user}"
  }

  os_profile_linux_config {
    disable_password_authentication = true

    ssh_keys = {
      path     = "/home/${var.ssh_user}/.ssh/authorized_keys"
      key_data = "${file("${var.ssh_public_key}")}"
    }
  }

  provisioner "local-exec" {
    command = "${data.template_file.chef-bootstrap-sentinel.*.rendered[count.index]}"
  }

  provisioner "remote-exec" {
    inline = ["nohup bash -c 'sudo chef-client &'"]

    connection {
      type        = "ssh"
      host        = "${azurerm_network_interface.sentinel.*.private_ip_address[count.index]}"
      user        = "${var.ssh_user}"
      private_key = "${file("${var.ssh_private_key}")}"
      timeout     = "10s"
    }
  }
}
