resource "azurerm_availability_set" "sidekiq-pullmirror" {
  name                         = "${format("sidekiq-pullmirror-%v", var.environment)}"
  location                     = "${var.location}"
  managed                      = true
  platform_update_domain_count = 20
  platform_fault_domain_count  = 3
  resource_group_name          = "${var.resource_group_name}"
}

resource "azurerm_network_interface" "sidekiq-pullmirror" {
  count                   = "${var.sidekiq_pullmirror_count}"
  name                    = "${format("sidekiq-pullmirror-%02d-%v-%v", count.index + 1, var.tier, var.environment)}"
  internal_dns_name_label = "${format("sidekiq-pullmirror-%02d-%v-%v", count.index + 1, var.tier, var.environment)}"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "${format("sidekiq-pullmirror-%02d-%v", count.index + 1, var.environment)}"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "${join(".", slice(split(".", var.address_prefix), 0, 3))}.${count.index + 81}"
  }
}

resource "aws_route53_record" "sidekiq-pullmirror" {
  count   = "${var.sidekiq_pullmirror_count}"
  zone_id = "${var.gitlab_com_zone_id}"
  name    = "${format("sidekiq-pullmirror-%02d.%v.%v.gitlab.com.", count.index + 1, var.tier, var.environment == "prod" ? "prd" : var.environment)}"
  type    = "A"
  ttl     = "300"
  records = ["${azurerm_network_interface.sidekiq-pullmirror.*.private_ip_address[count.index]}"]
}

data "template_file" "chef-bootstrap-sidekiq-pullmirror" {
  count    = "${var.sidekiq_pullmirror_count}"
  template = "${file("${path.root}/templates/chef-bootstrap.tpl")}"

  vars {
    ip_address     = "${azurerm_network_interface.sidekiq-pullmirror.*.private_ip_address[count.index]}"
    hostname       = "${format("sidekiq-pullmirror-%02d.%v.%v.gitlab.com", count.index + 1, var.tier, var.environment == "prod" ? "prd" : var.environment)}"
    chef_version   = "${var.chef_version}"
    chef_repo_dir  = "${var.chef_repo_dir}"
    ssh_user       = "${var.ssh_user}"
    ssh_key        = "${var.ssh_key}"
    chef_vaults    = "${var.chef_vaults}"
    chef_vault_env = "${var.chef_vault_env}"
  }
}

resource "azurerm_virtual_machine" "sidekiq-pullmirror" {
  count                         = "${var.sidekiq_pullmirror_count}"
  name                          = "${format("sidekiq-pullmirror-%02d.%v.%v.gitlab.com", count.index + 1, var.tier, var.environment == "prod" ? "prd" : var.environment)}"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  availability_set_id           = "${azurerm_availability_set.sidekiq-pullmirror.id}"
  network_interface_ids         = ["${azurerm_network_interface.sidekiq-pullmirror.*.id[count.index]}"]
  primary_network_interface_id  = "${azurerm_network_interface.sidekiq-pullmirror.*.id[count.index]}"
  vm_size                       = "${var.sidekiq_pullmirror_instance_type}"
  delete_os_disk_on_termination = true

  storage_image_reference {
    id = "${var.source_image}"
  }

  storage_os_disk {
    name              = "${format("osdisk-sidekiq-pullmirror-%02d-%v", count.index + 1, var.environment)}"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "${format("sidekiq-pullmirror-%02d.%v.%v.gitlab.com", count.index + 1, var.tier, var.environment == "prod" ? "prd" : var.environment)}"
    admin_username = "${var.first_user_username}"
    admin_username = "terraform"
  }

  os_profile_linux_config {
    disable_password_authentication = true
  }

  provisioner "local-exec" {
    command = "${data.template_file.chef-bootstrap-sidekiq-pullmirror.*.rendered[count.index]}"
  }

  provisioner "remote-exec" {
    inline = ["nohup bash -c 'sudo chef-client &'"]

    connection {
      type        = "ssh"
      host        = "${azurerm_network_interface.sidekiq-pullmirror.*.private_ip_address[count.index]}"
      user        = "${var.ssh_user}"
      private_key = "${file("${var.ssh_key}")}"
      timeout     = "10s"
    }
  }
}
