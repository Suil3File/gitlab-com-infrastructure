resource "azurerm_availability_set" "api-internal" {
  name                         = "${format("api-internal-%v", var.environment)}"
  location                     = "${var.location}"
  managed                      = true
  platform_update_domain_count = 20
  platform_fault_domain_count  = 3
  resource_group_name          = "${var.resource_group_name}"
}

resource "azurerm_network_interface" "api-internal" {
  count                   = "${var.count}"
  name                    = "${format("api-internal%02d-%v-%v", count.index + 1, var.tier, var.environment)}"
  internal_dns_name_label = "${format("api-internal%02d-%v-%v", count.index + 1, var.tier, var.environment)}"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "${format("api-internal%02d-%v", count.index + 1, var.environment)}"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "${join(".", slice(split(".", var.address_prefix), 0, 3))}.${count.index + 101}"
  }
}

resource "aws_route53_record" "api-internal" {
  count   = "${var.count}"
  zone_id = "${var.gitlab_com_zone_id}"
  name    = "${format("api-internal%02d.%v.%v.gitlab.com.", count.index + 1, var.tier, var.environment == "prod" ? "prd" : var.environment)}"
  type    = "A"
  ttl     = "300"
  records = ["${azurerm_network_interface.api-internal.*.private_ip_address[count.index]}"]
}

data "template_file" "chef-bootstrap-api-internal" {
  count    = "${var.count}"
  template = "${file("${path.root}/templates/chef-bootstrap.tpl")}"

  vars {
    ip_address          = "${azurerm_network_interface.api-internal.*.private_ip_address[count.index]}"
    hostname            = "${format("api-internal%02d.%v.%v.gitlab.com", count.index + 1, var.tier, var.environment == "prod" ? "prd" : var.environment)}"
    chef_repo_dir       = "${var.chef_repo_dir}"
    first_user_username = "${var.first_user_username}"
    first_user_password = "${var.first_user_password}"
    chef_vaults         = "${var.chef_vaults}"
    chef_vault_env      = "${var.chef_vault_env}"
  }
}

resource "azurerm_virtual_machine" "api-internal" {
  count                         = "${var.count}"
  name                          = "${format("api-internal%02d.%v.%v.gitlab.com", count.index + 1, var.tier, var.environment == "prod" ? "prd" : var.environment)}"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  availability_set_id           = "${azurerm_availability_set.api-internal.id}"
  network_interface_ids         = ["${azurerm_network_interface.api-internal.*.id[count.index]}"]
  primary_network_interface_id  = "${azurerm_network_interface.api-internal.*.id[count.index]}"
  vm_size                       = "${var.instance_type}"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "${format("osdisk-api-internal%02d-%v", count.index + 1, var.environment)}"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "${format("api-internal%02d.%v.%v.gitlab.com", count.index + 1, var.tier, var.environment == "prod" ? "prd" : var.environment)}"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  provisioner "local-exec" {
    command = "${data.template_file.chef-bootstrap-api-internal.*.rendered[count.index]}"
  }

  provisioner "remote-exec" {
    inline = ["nohup bash -c 'sudo chef-client &'"]

    connection {
      type     = "ssh"
      host     = "${azurerm_network_interface.api-internal.*.private_ip_address[count.index]}"
      user     = "${var.first_user_username}"
      password = "${var.first_user_password}"
      timeout  = "10s"
    }
  }
}
