resource "google_compute_address" "external" {
  count        = "${var.use_external_ip ? var.node_count : 0}"
  name         = "${format("%v-%02d-%v-%v-static-ip", var.name, count.index + 1, var.tier, var.environment)}"
  address_type = "EXTERNAL"
}

# This works around terraform limitation of using resources as arrays that
# don't necessary exist. https://github.com/hashicorp/terraform/issues/9858#issuecomment-386431631

locals {
  external_ips = "${concat(google_compute_address.external.*.address, list(""))}"
}

resource "google_compute_region_backend_service" "default" {
  count    = "${var.backend_service_type == "regional" ? 1 : 0}"
  name     = "${format("%v-%v-regional", var.environment, var.name)}"
  protocol = "TCP"

  backend {
    group = "${google_compute_instance_group.default.*.self_link[0]}"
  }

  backend {
    group = "${google_compute_instance_group.default.*.self_link[1]}"
  }

  backend {
    group = "${google_compute_instance_group.default.*.self_link[2]}"
  }

  health_checks = ["${var.health_check == "http" ? google_compute_health_check.http.self_link : google_compute_health_check.tcp.self_link }"]
}

resource "google_compute_backend_service" "iap" {
  count     = "${var.backend_service_type == "regional" ? 0 : 1}"
  name      = "${format("%v-%v", var.environment, var.name)}"
  protocol  = "${var.backend_protocol}"
  port_name = "${var.name}"

  backend {
    group = "${google_compute_instance_group.default.*.self_link[0]}"
  }

  backend {
    group = "${google_compute_instance_group.default.*.self_link[1]}"
  }

  backend {
    group = "${google_compute_instance_group.default.*.self_link[2]}"
  }

  health_checks = ["${var.health_check == "http" ? google_compute_health_check.http.self_link : google_compute_health_check.tcp.self_link }"]

  iap {
    oauth2_client_secret = "${var.oauth2_client_secret}"
    oauth2_client_id     = "${var.oauth2_client_id}"
  }
}

resource "google_compute_health_check" "tcp" {
  name = "${format("%v-%v-tcp", var.environment, var.name)}"

  tcp_health_check {
    port = "${var.health_check_port == "use service port" ? var.service_port : var.health_check_port}"
  }
}

resource "google_compute_health_check" "http" {
  name = "${format("%v-%v-http", var.environment, var.name)}"

  http_health_check {
    port         = "${var.health_check_port == "use service port" ? var.service_port : var.health_check_port}"
    request_path = "${var.service_path}"
  }
}

# Add one instance group per zone
# and only select the appropriate instances
# for each one

resource "google_compute_instance_group" "default" {
  count       = "${length(data.google_compute_zones.available.names)}"
  name        = "${format("%v-%v-%v", var.environment, var.name, data.google_compute_zones.available.names[count.index])}"
  description = "Instance group for monitoring VM."
  zone        = "${data.google_compute_zones.available.names[count.index]}"

  named_port {
    name = "${var.name}"
    port = "${var.service_port}"
  }

  named_port {
    name = "http"
    port = "80"
  }

  named_port {
    name = "https"
    port = "443"
  }

  named_port {
    name = "${var.name}"
    port = "${var.service_port}"
  }

  # This filters the full set of instances to only ones for the appropriate zone.
  instances = ["${matchkeys(google_compute_instance.default.*.self_link, google_compute_instance.default.*.zone, list(data.google_compute_zones.available.names[count.index]))}"]
}

resource "google_compute_disk" "default" {
  count   = "${var.node_count}"
  project = "${var.project}"
  name    = "${format("%v-%02d-%v-%v-data", var.name, count.index + 1, var.tier, var.environment)}"
  zone    = "${var.zone != "" ? var.zone : element(data.google_compute_zones.available.names, count.index + 1)}"
  size    = "${var.data_disk_size}"
  type    = "${var.data_disk_type}"

  labels {
    environment  = "${var.environment}"
    pet_name     = "${var.name}"
    do_snapshots = "true"
  }

  lifecycle {
    ignore_changes = ["snapshot"]
  }
}

resource "google_compute_disk" "log_disk" {
  project = "${var.project}"
  count   = "${var.node_count}"
  name    = "${format("%v-%02d-%v-%v-log", var.name, count.index + 1, var.tier, var.environment)}"
  zone    = "${var.zone != "" ? var.zone : element(data.google_compute_zones.available.names, count.index + 1)}"
  size    = "${var.log_disk_size}"
  type    = "${var.log_disk_type}"

  labels {
    environment = "${var.environment}"
  }
}

resource "google_compute_instance" "default" {
  count        = "${var.node_count}"
  name         = "${format("%v-%02d-%v-%v", var.name, count.index + 1, var.tier, var.environment)}"
  machine_type = "${var.machine_type}"

  metadata = {
    "CHEF_URL"                = "${var.chef_provision.["server_url"]}"
    "CHEF_VERSION"            = "${var.chef_provision.["version"]}"
    "CHEF_ENVIRONMENT"        = "${var.environment}"
    "CHEF_NODE_NAME"          = "${var.use_new_node_name ? format("%v-%02d-%v-%v.c.%v.internal", var.name, count.index + 1, var.tier, var.environment, var.project) : format("%v-%02d.%v.%v.%v", var.name, count.index + 1, var.tier, var.environment, var.dns_zone_name)}"
    "GL_KERNEL_VERSION"       = "${var.kernel_version}"
    "CHEF_RUN_LIST"           = "${var.chef_run_list}"
    "CHEF_DNS_ZONE_NAME"      = "${var.dns_zone_name}"
    "CHEF_PROJECT"            = "${var.project}"
    "CHEF_BOOTSTRAP_BUCKET"   = "${var.chef_provision.["bootstrap_bucket"]}"
    "CHEF_BOOTSTRAP_KEYRING"  = "${var.chef_provision.["bootstrap_keyring"]}"
    "CHEF_BOOTSTRAP_KEY"      = "${var.chef_provision.["bootstrap_key"]}"
    "GL_PERSISTENT_DISK_PATH" = "${var.persistent_disk_path}"
    "GL_FORMAT_DATA_DISK"     = "${var.format_data_disk}"
    "block-project-ssh-keys"  = "${var.block_project_ssh_keys}"
    "enable-oslogin"          = "${var.enable_oslogin}"
    "shutdown-script"         = "${file("${path.module}/../../../scripts/google/teardown-v1.sh")}"
  }

  metadata_startup_script = "${file("${path.module}/../../../scripts/google/bootstrap-v${var.bootstrap_version}.sh")}"
  project                 = "${var.project}"

  zone = "${var.zone != "" ? var.zone : element(data.google_compute_zones.available.names, count.index + 1)}"

  service_account {
    // this should be the instance under which the instance should be running, rather than the one creating it...
    email = "${var.service_account_email}"

    // all the defaults plus cloudkms to access kms
    scopes = [
      "https://www.googleapis.com/auth/cloud.useraccounts.readonly",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring.write",
      "https://www.googleapis.com/auth/pubsub",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/trace.append",
      "https://www.googleapis.com/auth/cloudkms",
      "https://www.googleapis.com/auth/compute.readonly",
    ]
  }

  scheduling {
    preemptible = "${var.preemptible}"
  }

  boot_disk {
    auto_delete = true

    initialize_params {
      image = "${var.os_boot_image}"
      size  = "${var.os_disk_size}"
      type  = "${var.os_disk_type}"
    }
  }

  attached_disk {
    source = "${google_compute_disk.default.*.self_link[count.index]}"
  }

  attached_disk {
    source      = "${google_compute_disk.log_disk.*.self_link[count.index]}"
    device_name = "log"
  }

  network_interface {
    subnetwork = "${var.subnetwork_name != "" ? var.subnetwork_name : join("", google_compute_subnetwork.subnetwork.*.name) }"

    access_config = {
      nat_ip = "${var.use_external_ip ? element(local.external_ips, count.index) : "" }"
    }
  }

  labels {
    environment = "${var.environment}"
    pet_name    = "${var.name}"
  }

  tags = [
    "${var.name}",
    "${var.environment}",
  ]

  provisioner "local-exec" {
    when    = "destroy"
    command = "knife node delete ${format("%v.%v.%v.%v", var.name, var.tier, var.environment, var.dns_zone_name)} -y; knife client delete ${format("%v.%v.%v.%v", var.name, var.tier, var.environment, var.dns_zone_name)} -y; exit 0"
  }
}
