data "google_compute_lb_ip_ranges" "ranges" {}

resource "aws_route53_record" "default" {
  zone_id = "${var.gitlab_zone_id}"
  name    = "${var.web_ip_fqdn}"
  type    = "A"
  ttl     = "300"
  records = ["${google_compute_global_address.default.address}"]
}

resource "google_compute_global_address" "default" {
  name = "${format("%v-%v", var.environment, var.name)}"
}

resource "google_compute_global_forwarding_rule" "default" {
  name       = "${format("%v-%v", var.environment, var.name)}"
  target     = "${google_compute_target_https_proxy.default.self_link}"
  port_range = "443"
  ip_address = "${google_compute_global_address.default.address}"
}

resource "google_compute_target_https_proxy" "default" {
  name             = "${format("%v-%v", var.environment, var.name)}"
  description      = "https proxy for performance"
  ssl_certificates = ["${var.cert_link}"]
  url_map          = "${google_compute_url_map.default.self_link}"
}

resource "google_compute_firewall" "default" {
  name    = "${format("%v-%v", var.environment, var.name)}"
  network = "${var.environment}"

  allow {
    protocol = "tcp"
    ports    = ["${var.service_ports}"]
  }

  source_ranges = ["${concat(data.google_compute_lb_ip_ranges.ranges.network, data.google_compute_lb_ip_ranges.ranges.http_ssl_tcp_internal)}"]
  target_tags   = ["${var.name}"]
}

resource "google_compute_url_map" "default" {
  name            = "${format("%v-%v", var.environment, var.name)}"
  default_service = "${var.backend_service_link}"

  host_rule {
    hosts        = ["${var.web_ip_fqdn}"]
    path_matcher = "${var.name}"
  }

  path_matcher {
    name            = "${var.name}"
    default_service = "${var.backend_service_link}"

    path_rule {
      paths   = ["/*"]
      service = "${var.backend_service_link}"
    }
  }
}
