data "google_compute_lb_ip_ranges" "ranges" {}

resource "aws_route53_record" "default" {
  count   = "${var.external ? length(var.fqdns) : 0}"
  zone_id = "${var.gitlab_zone_id}"
  name    = "${var.fqdns[count.index]}"
  type    = "A"
  ttl     = "300"
  records = ["${google_compute_address.default.address}"]
}

resource "aws_route53_record" "internal" {
  count   = "${var.external ? 0 : length(var.fqdns)}"
  zone_id = "${var.gitlab_zone_id}"
  name    = "${var.fqdns[count.index]}"
  type    = "A"
  ttl     = "300"
  records = ["${google_compute_forwarding_rule.internal.ip_address}"]
}

resource "google_compute_address" "default" {
  name         = "${format("%v-%v", var.environment, var.name)}"
  project      = "${var.project}"
  region       = "${var.region}"
  address_type = "${var.external ? "EXTERNAL" : "INTERNAL"}"
}

resource "google_compute_firewall" "default" {
  name    = "${format("%v-%v", var.environment, var.name)}"
  network = "${var.environment}"

  allow {
    protocol = "tcp"
    ports    = ["${var.health_check_ports}"]
  }

  source_ranges = ["${concat(data.google_compute_lb_ip_ranges.ranges.network, data.google_compute_lb_ip_ranges.ranges.http_ssl_tcp_internal)}"]
  target_tags   = ["${var.targets}"]
}

resource "google_compute_forwarding_rule" "default" {
  count                 = "${var.external ? var.lb_count : 0}"
  name                  = "${format("%v-%v-%v", var.environment, var.name, var.names[count.index])}"
  project               = "${var.project}"
  region                = "${var.region}"
  target                = "${google_compute_target_pool.default.*.self_link[count.index]}"
  load_balancing_scheme = "${var.external ? "EXTERNAL" : "INTERNAL"}"
  port_range            = "${var.forwarding_port_ranges[count.index]}"
  ip_address            = "${google_compute_address.default.address}"
}

resource "google_compute_forwarding_rule" "internal" {
  count                 = "${var.external ? 0 : 1}"
  name                  = "${format("%v-%v", var.environment, var.name)}"
  project               = "${var.project}"
  region                = "${var.region}"
  backend_service       = "${var.backend_service}"
  load_balancing_scheme = "${var.external ? "EXTERNAL" : "INTERNAL"}"
  ports                 = ["${var.forwarding_port_ranges}"]
  network               = "${var.environment}"
  subnetwork            = "${var.subnetwork_self_link}"
  service_label         = "i"
}

resource "google_compute_target_pool" "default" {
  count            = "${var.external ? var.lb_count : 0}"
  name             = "${format("%v-%v-%v", var.environment, var.name, var.names[count.index])}"
  project          = "${var.project}"
  region           = "${var.region}"
  session_affinity = "${var.session_affinity}"
  instances        = ["${var.instances}"]

  health_checks = [
    "${google_compute_http_health_check.default.*.self_link[count.index]}",
  ]
}

resource "google_compute_http_health_check" "default" {
  count   = "${var.lb_count}"
  name    = "${format("%v-%v-%v", var.environment, var.name, var.names[count.index])}"
  project = "${var.project}"
  port    = "${var.health_check_ports[count.index]}"

  # Because request_paths can be empty, we use this element/concat hack, see https://stackoverflow.com/a/47415781/1856239
  request_path        = "${length(var.health_check_request_paths) > 0 ? element(concat(var.health_check_request_paths, list("")), count.index) : format("/-/available-%v", var.names[count.index])}"
  timeout_sec         = 2
  check_interval_sec  = 2
  healthy_threshold   = 2
  unhealthy_threshold = 2
}
