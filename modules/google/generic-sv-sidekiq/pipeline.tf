resource "google_compute_disk" "pipeline_log_disk" {
  project = "${var.project}"
  count   = "${var.sidekiq_pipeline_count}"
  name    = "${format("%v-pipeline-%02d-%v-%v-log", var.name, count.index + 1, var.tier, var.environment)}"
  zone    = "${var.zone != "" ? var.zone : element(data.google_compute_zones.available.names, count.index + 1)}"
  size    = "${var.log_disk_size}"
  type    = "${var.log_disk_type}"

  labels {
    environment = "${var.environment}"
  }
}

resource "google_compute_instance" "sidekiq_pipeline" {
  count        = "${var.sidekiq_pipeline_count}"
  name         = "${format("%v-pipeline-%02d-%v-%v", var.name, count.index + 1, var.tier, var.environment)}"
  machine_type = "${var.sidekiq_pipeline_instance_type}"

  metadata = {
    "CHEF_URL"               = "${var.chef_provision.["server_url"]}"
    "CHEF_VERSION"           = "${var.chef_provision.["version"]}"
    "CHEF_NODE_NAME"         = "${var.use_new_node_name ? format("%v-pipeline-%02d-%v-%v.c.%v.internal", var.name, count.index + 1, var.tier, var.environment, var.project) : format("%v-pipeline-%02d.%v.%v.%v", var.name, count.index + 1, var.tier, var.environment, var.dns_zone_name)}"
    "GL_KERNEL_VERSION"      = "${var.kernel_version}"
    "CHEF_ENVIRONMENT"       = "${var.environment}"
    "CHEF_RUN_LIST"          = "\"role[${var.environment}-base-be-sidekiq-pipeline]\""
    "CHEF_DNS_ZONE_NAME"     = "${var.dns_zone_name}"
    "CHEF_PROJECT"           = "${var.project}"
    "CHEF_BOOTSTRAP_BUCKET"  = "${var.chef_provision.["bootstrap_bucket"]}"
    "CHEF_BOOTSTRAP_KEYRING" = "${var.chef_provision.["bootstrap_keyring"]}"
    "CHEF_BOOTSTRAP_KEY"     = "${var.chef_provision.["bootstrap_key"]}"
    "block-project-ssh-keys" = "${var.block_project_ssh_keys}"
    "enable-oslogin"         = "${var.enable_oslogin}"
    "shutdown-script"        = "${file("${path.module}/../../../scripts/google/teardown-v1.sh")}"
  }

  metadata_startup_script = "${file("${path.module}/../../../scripts/google/bootstrap-v${var.bootstrap_version}.sh")}"
  project                 = "${var.project}"
  zone                    = "${var.zone != "" ? var.zone : element(data.google_compute_zones.available.names, count.index + 1)}"

  service_account {
    // this should be the instance under which the instance should be running, rather than the one creating it...
    email = "${var.service_account_email}"

    // all the defaults plus cloudkms to access kms
    scopes = [
      "https://www.googleapis.com/auth/cloud.useraccounts.readonly",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring.write",
      "https://www.googleapis.com/auth/pubsub",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/trace.append",
      "https://www.googleapis.com/auth/cloudkms",
      "https://www.googleapis.com/auth/compute.readonly",
    ]
  }

  scheduling {
    preemptible = "${var.preemptible}"
  }

  boot_disk {
    auto_delete = true

    initialize_params {
      image = "${var.os_boot_image}"
      size  = "${var.os_disk_size}"
      type  = "${var.os_disk_type}"
    }
  }

  attached_disk {
    source      = "${google_compute_disk.pipeline_log_disk.*.self_link[count.index]}"
    device_name = "log"
  }

  network_interface {
    subnetwork    = "${google_compute_subnetwork.sidekiq.name}"
    access_config = {}
  }

  labels {
    environment = "${var.environment}"
    pet_name    = "${var.name}-pipeline"
  }

  tags = [
    "${var.name}",
    "${var.name}-pipeline",
    "${var.environment}",
  ]
}

output "sidekiq_pipeline_self_link" {
  value = "${google_compute_instance.sidekiq_pipeline.*.self_link}"
}
