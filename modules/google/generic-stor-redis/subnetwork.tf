resource "google_compute_subnetwork" "subnetwork" {
  count                    = "${signum(var.redis_count + var.sentinel_count) > 0 ? 1 : 0}"
  name                     = "${format("%v-%v", var.name, var.environment)}"
  network                  = "${var.vpc}"
  project                  = "${var.project}"
  region                   = "${var.region}"
  ip_cidr_range            = "${var.ip_cidr_range}"
  private_ip_google_access = true
}
