variable "bootstrap_version" {
  description = "version of the bootstrap script"
  default     = 1
}

variable "disk_size" {
  type        = "string"
  description = "The size of the disk for the worker nodes"
}

variable "dns_zone_name" {
  type        = "string"
  description = "The GCP name of the DNS zone to use for this environment"
}

variable "environment" {
  type        = "string"
  description = "The environment name"
}

variable "initial_node_count" {
  type        = "string"
  description = "The number of nodes for each zone"
}

variable "ip_cidr_range" {
  type        = "string"
  description = "The IP range"
}

variable "kubernetes_version" {
  type        = "string"
  description = "The Kubernetes version. Defaults to latest available"
  default     = ""
}

variable "machine_type" {
  type        = "string"
  description = "The type of VM for the worker nodes"
}

variable "master_auth_password" {
  type        = "string"
  description = "The password to use for HTTP basic authentication when accessing the Kubernetes master endpoint"
}

variable "master_auth_username" {
  type        = "string"
  description = "The username to use for HTTP basic authentication when accessing the Kubernetes master endpoint"
}

variable "name" {
  type        = "string"
  description = "The GKE cluster name"
}

variable "preemptible" {
  type        = "string"
  description = "Use preemptible instances for this cluster"
  default     = false
}

variable "project" {
  type        = "string"
  description = "The project name"
}

variable "public_ports" {
  type        = "list"
  description = "The list of ports that should be publicly reachable"
  default     = []
}

variable "region" {
  type        = "string"
  description = "The target region"
}

variable "vpc" {
  type        = "string"
  description = "The target network"
}
