output "instances_self_link" {
  value = "${google_compute_instance.instance_with_attached_disk.*.self_link}"
}

output "instance_public_ips" {
  value = "${google_compute_instance.instance_with_attached_disk.*.network_interface.0.access_config.0.assigned_nat_ip}"
}
