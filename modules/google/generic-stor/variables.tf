variable "use_external_ip" {
  default = false
}

variable "kernel_version" {
  default = ""
}

variable "use_new_node_name" {
  default = false
}

variable "allow_stopping_for_update" {
  description = "wether Terraform is allowed to stop the instance to update its properties"
  default     = "false"
}

variable "block_project_ssh_keys" {
  type        = "string"
  description = "Whether to block project level SSH keys"
  default     = "TRUE"
}

variable "bootstrap_version" {
  description = "version of the bootstrap script"
  default     = 1
}

variable "persistent_disk_path" {
  type        = "string"
  description = "default location for disk mount"
  default     = "/var/opt/gitlab"
}

variable "chef_init_run_list" {
  type        = "string"
  default     = ""
  description = "run_list for the node in chef that are ran on the first boot only"
}

variable "chef_provision" {
  type        = "map"
  description = "Configuration details for chef server"
}

variable "chef_run_list" {
  type        = "string"
  description = "run_list for the node in chef"
}

variable "data_disk_size" {
  type        = "string"
  description = "The size of the data disk"
  default     = 20
}

variable "data_disk_type" {
  type        = "string"
  description = "The type of the data disk"
  default     = "pd-standard"
}

variable "dns_zone_name" {
  type        = "string"
  description = "The GCP name of the DNS zone to use for this environment"
}

variable "enable_oslogin" {
  type        = "string"
  description = "Whether to enable OS Login GCP feature"

  # Note: setting this to TRUE breaks chef!
  # https://gitlab.com/gitlab-com/gitlab-com-infrastructure/merge_requests/297#note_66690562
  default = "FALSE"
}

variable "environment" {
  type        = "string"
  description = "The environment name"
}

variable "log_disk_size" {
  type        = "string"
  description = "The size of the log disk"
  default     = 50
}

variable "log_disk_type" {
  type        = "string"
  description = "The type of the log disk"
  default     = "pd-standard"
}

variable "format_data_disk" {
  type        = "string"
  description = "Force formatting of the persistent disk."
  default     = "false"
}

variable "ip_cidr_range" {
  type        = "string"
  description = "The IP range"
}

variable "machine_type" {
  type        = "string"
  description = "The machine size"
}

variable "name" {
  type        = "string"
  description = "The pet name"
}

variable "node_count" {
  type        = "string"
  description = "The nodes count"
}

variable "os_boot_image" {
  type        = "string"
  description = "The OS image to boot"
  default     = "ubuntu-os-cloud/ubuntu-1604-xenial-v20180122"
}

variable "os_disk_size" {
  type        = "string"
  description = "The OS disk size in GiB"
  default     = 20
}

variable "os_disk_type" {
  type        = "string"
  description = "The OS disk type"
  default     = "pd-standard"
}

variable "preemptible" {
  type        = "string"
  description = "Use preemptible instances for this pet"
  default     = "false"
}

variable "project" {
  type        = "string"
  description = "The project name"
}

variable "public_ports" {
  type        = "list"
  description = "The list of ports that should be publicly reachable"
  default     = []
}

variable "region" {
  type        = "string"
  description = "The target region"
}

variable "service_account_email" {
  type        = "string"
  description = "Service account emails under which the instance is running"
}

variable "tier" {
  type        = "string"
  description = "The tier for this service"
}

variable "vpc" {
  type        = "string"
  description = "The target network"
}

variable "zone" {
  type    = "string"
  default = ""
}
