resource "google_compute_address" "external" {
  count        = "${var.use_external_ip ? var.node_count : 0}"
  name         = "${format("%v-%02d-%v-%v-static-ip", var.name, count.index + 1, var.tier, var.environment)}"
  address_type = "EXTERNAL"
}

# This works around terraform limitation of using resources as arrays that
# don't necessary exist. https://github.com/hashicorp/terraform/issues/9858#issuecomment-386431631

locals {
  external_ips = "${concat(google_compute_address.external.*.address, list(""))}"
}

resource "google_compute_address" "static-ip-address" {
  count        = "${var.node_count}"
  name         = "${format("%v-%02d-%v-%v-static-ip", var.name, count.index + 1 + 100, var.tier, var.environment)}"
  address_type = "INTERNAL"

  address    = "${replace(var.ip_cidr_range, "/\\d+\\/\\d+$/", count.index + 1 + 100)}"
  subnetwork = "${google_compute_subnetwork.subnetwork.self_link}"
}

resource "google_compute_disk" "data_disk" {
  project = "${var.project}"
  count   = "${var.node_count}"
  name    = "${format("%v-%02d-%v-%v-data", var.name, count.index + 1, var.tier, var.environment)}"
  zone    = "${var.zone != "" ? var.zone : element(data.google_compute_zones.available.names, count.index + 1)}"
  size    = "${var.data_disk_size}"
  type    = "${var.data_disk_type}"

  labels {
    environment  = "${var.environment}"
    pet_name     = "${var.name}"
    do_snapshots = "true"
  }

  lifecycle {
    ignore_changes = ["snapshot"]
  }
}

resource "google_compute_disk" "log_disk" {
  project = "${var.project}"
  count   = "${var.node_count}"
  name    = "${format("%v-%02d-%v-%v-log", var.name, count.index + 1, var.tier, var.environment)}"
  zone    = "${var.zone != "" ? var.zone : element(data.google_compute_zones.available.names, count.index + 1)}"
  size    = "${var.log_disk_size}"
  type    = "${var.log_disk_type}"

  labels {
    environment = "${var.environment}"
  }
}

resource "google_compute_instance" "instance_with_attached_disk" {
  allow_stopping_for_update = "${var.allow_stopping_for_update}"

  count        = "${var.node_count}"
  name         = "${format("%v-%02d-%v-%v", var.name, count.index + 1, var.tier, var.environment)}"
  machine_type = "${var.machine_type}"

  metadata = {
    "CHEF_URL"                = "${var.chef_provision.["server_url"]}"
    "CHEF_VERSION"            = "${var.chef_provision.["version"]}"
    "CHEF_ENVIRONMENT"        = "${var.environment}"
    "CHEF_NODE_NAME"          = "${var.use_new_node_name ? format("%v-%02d-%v-%v.c.%v.internal", var.name, count.index + 1, var.tier, var.environment, var.project) : format("%v-%02d.%v.%v.%v", var.name, count.index + 1, var.tier, var.environment, var.dns_zone_name)}"
    "GL_KERNEL_VERSION"       = "${var.kernel_version}"
    "CHEF_RUN_LIST"           = "${var.chef_run_list}"
    "CHEF_DNS_ZONE_NAME"      = "${var.dns_zone_name}"
    "CHEF_PROJECT"            = "${var.project}"
    "CHEF_BOOTSTRAP_BUCKET"   = "${var.chef_provision.["bootstrap_bucket"]}"
    "CHEF_BOOTSTRAP_KEYRING"  = "${var.chef_provision.["bootstrap_keyring"]}"
    "CHEF_BOOTSTRAP_KEY"      = "${var.chef_provision.["bootstrap_key"]}"
    "CHEF_INIT_RUN_LIST"      = "${var.chef_init_run_list}"
    "GL_PERSISTENT_DISK_PATH" = "${var.persistent_disk_path}"
    "GL_FORMAT_DATA_DISK"     = "${var.format_data_disk}"
    "block-project-ssh-keys"  = "${var.block_project_ssh_keys}"
    "enable-oslogin"          = "${var.enable_oslogin}"
    "shutdown-script"         = "${file("${path.module}/../../../scripts/google/teardown-v1.sh")}"
  }

  metadata_startup_script = "${file("${path.module}/../../../scripts/google/bootstrap-v${var.bootstrap_version}.sh")}"
  project                 = "${var.project}"
  zone                    = "${var.zone != "" ? var.zone : element(data.google_compute_zones.available.names, count.index + 1)}"

  service_account {
    // this should be the instance under which the instance should be running, rather than the one creating it...
    email = "${var.service_account_email}"

    // all the defaults plus cloudkms to access kms
    scopes = [
      "https://www.googleapis.com/auth/cloud.useraccounts.readonly",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring.write",
      "https://www.googleapis.com/auth/pubsub",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/trace.append",
      "https://www.googleapis.com/auth/cloudkms",
      "https://www.googleapis.com/auth/compute.readonly",
    ]
  }

  scheduling {
    preemptible = "${var.preemptible}"
  }

  boot_disk {
    auto_delete = true

    initialize_params {
      image = "${var.os_boot_image}"
      size  = "${var.os_disk_size}"
      type  = "${var.os_disk_type}"
    }
  }

  attached_disk {
    source = "${google_compute_disk.data_disk.*.self_link[count.index]}"
  }

  attached_disk {
    source      = "${google_compute_disk.log_disk.*.self_link[count.index]}"
    device_name = "log"
  }

  network_interface {
    subnetwork = "${google_compute_subnetwork.subnetwork.name}"
    address    = "${google_compute_address.static-ip-address.*.address[count.index]}"

    access_config = {
      nat_ip = "${var.use_external_ip ? element(local.external_ips, count.index) : "" }"
    }
  }

  labels {
    environment = "${var.environment}"
    pet_name    = "${var.name}"
  }

  tags = [
    "${var.name}",
    "${var.environment}",
  ]

  provisioner "local-exec" {
    when    = "destroy"
    command = "knife node delete ${format("%v-%02d.%v.%v.%v", var.name, count.index + 1, var.tier, var.environment, var.dns_zone_name)} -y; knife client delete ${format("%v-%02d.%v.%v.%v", var.name, count.index + 1, var.tier, var.environment, var.dns_zone_name)} -y; exit 0"
  }
}
