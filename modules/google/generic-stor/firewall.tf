resource "google_compute_firewall" "public" {
  count   = "${length(var.public_ports) > 0 ? 1 : 0}"
  name    = "${format("%v-%v", var.name, var.environment)}"
  network = "${var.vpc}"

  allow {
    protocol = "tcp"
    ports    = ["${var.public_ports}"]
  }

  source_ranges = ["0.0.0.0/0"]

  target_tags = ["${var.name}"]
}
