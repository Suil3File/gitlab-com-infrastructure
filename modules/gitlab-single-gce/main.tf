variable "location" {}
variable "machine_type" {}

variable "name" {
  default = "single"
}

variable "disk_size" {
  default = "1024"
}

variable "backup_aws_access_key" {}
variable "backup_aws_secret_key" {}
variable "prod_ip" {}

variable "vpn_ips" {
  default = ["10.0.0.1", "10.0.0.2"]
}

variable "prometheus_ip" {
  default = "13.68.87.12"
}

resource "google_compute_disk" "single" {
  name  = "${var.name}-disk"
  type  = "pd-ssd"
  size  = "${var.disk_size}"
  zone  = "${var.location}"
  image = "debian-8-jessie-v20170523"
}

resource "google_compute_instance" "single" {
  name         = "${var.name}-instance"
  machine_type = "${var.machine_type}"
  zone         = "${var.location}"
  tags         = ["${var.name}"]

  boot_disk {
    initialize_params {
      image = "ubuntu-1604-xenial-v20170811"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }

  metadata_startup_script = "${file("${path.module}/files/bootstrap.bash")}"

  attached_disk {
    source      = "${google_compute_disk.single.self_link}"
    device_name = "gitlab_var"
  }
}

output "public_ip" {
  value = "${google_compute_instance.single.network_interface.0.access_config.0.assigned_nat_ip}"
}
