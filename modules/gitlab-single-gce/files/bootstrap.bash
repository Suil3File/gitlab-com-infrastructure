#!/bin/bash
set -ex
exec &> >(tee -a "/tmp/bootstrap.log")

useradd -u 1100 git # this uid is used for the restore
export DEBIAN_FRONTEND=noninteractive

mkdir -p /var/opt/gitlab
mkfs.ext4 -F /dev/disk/by-id/google-gitlab_var
mount /dev/disk/by-id/google-gitlab_var /var/opt/gitlab

# Set apt config, update repos and disable postfix prompt
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
debconf-set-selections <<< "postfix postfix/main_mailer_type string 'No configuration'"

# install everything in one go
apt-get -y install daemontools lzop gcc make python3 virtualenv python3-dev libssl-dev gitlab-ee ca-certificates postfix
gitlab-ctl reconfigure

# stop postgres just after reconfig
gitlab-ctl stop postgresql

sed -i 's/^max_replication_slots = 0/max_replication_slots = 100/' /var/opt/gitlab/postgresql/data/postgresql.conf
gitlab-ctl start postgresql
