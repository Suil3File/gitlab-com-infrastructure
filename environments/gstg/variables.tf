variable "oauth2_client_id_monitoring" {}
variable "oauth2_client_secret_monitoring" {}

variable "gitlab_net_zone_id" {}
variable "gitlab_com_zone_id" {}
variable "gitlab_io_zone_id" {}

variable "default_kernel_version" {
  default = "4.10.0-1009"
}

##################
# Network Peering
##################

variable "network_ops" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-ops/global/networks/ops"
}

variable "network_env" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-staging-1/global/networks/gstg"
}

#######################
# pubsubbeat config
#######################

variable "pubsubbeats" {
  type = "map"

  default = {
    "names"         = ["gitaly", "haproxy", "pages", "postgres", "production", "system", "workhorse", "geo", "sidekiq", "api", "nginx", "gitlab-shell", "shell", "rails", "unstructured", "unicorn", "application", "registry", "redis"]
    "machine_types" = ["n1-standard-1", "n1-standard-1", "n1-standard-1", "n1-standard-1", "n1-standard-1", "n1-standard-1", "n1-standard-1", "n1-standard-1", "n1-standard-4", "n1-standard-1", "n1-standard-1", "n1-standard-1", "n1-standard-1", "n1-standard-1", "n1-standard-1", "n1-standard-1", "n1-standard-1", "n1-standard-1", "n1-standard-1"]
  }
}

######################

variable "base_chef_run_list" {
  default = "\"role[gitlab]\",\"recipe[gitlab_users::default]\",\"recipe[gitlab_sudo::default]\",\"recipe[gitlab-server::bashrc]\""
}

variable "empty_chef_run_list" {
  default = "\"\""
}

variable "dns_zone_name" {
  default = "gitlab.com"
}

variable "monitoring_hosts" {
  type = "map"

  default = {
    "names" = ["alerts", "prometheus", "prometheus-app"]
    "ports" = [9093, 9090, 9090]
  }
}

#### GCP load balancing

# The top level domain record for the GitLab deployment.
# For production this should be set to "gitlab.com"

variable "lb_fqdns" {
  type    = "list"
  default = ["gstg.gitlab.com", "registry.staging.gitlab.com"]
}

#####

variable "lb_fqdns_altssh" {
  type    = "list"
  default = ["altssh.gstg.gitlab.com"]
}

variable "lb_fqdns_pages" {
  type    = "list"
  default = ["*.pages.gstg.gitlab.io"]
}

variable "lb_fqdns_bastion" {
  type    = "list"
  default = ["lb-bastion.gstg.gitlab.com"]
}

variable "lb_fqdns_internal" {
  type    = "list"
  default = ["int.gstg.gitlab.net"]
}

variable "lb_fqdns_internal_pgbouncer" {
  type    = "list"
  default = ["pgbouncer.int.gstg.gitlab.net"]
}

#
# For every name there must be a corresponding
# forwarding port range and health check port
#

variable "tcp_lbs" {
  type = "map"

  default = {
    "names"                  = ["http", "https", "ssh"]
    "forwarding_port_ranges" = ["80", "443", "22"]
    "health_check_ports"     = ["8001", "8002", "8003"]
  }
}

variable "tcp_lbs_internal" {
  type = "map"

  default = {
    "names"                  = ["http-internal", "https-internal", "ssh-internal"]
    "forwarding_port_ranges" = ["80", "443", "22"]
    "health_check_ports"     = ["8001", "8002", "8003"]
  }
}

variable "tcp_lbs_pages" {
  type = "map"

  default = {
    "names"                  = ["http", "https"]
    "forwarding_port_ranges" = ["80", "443"]
    "health_check_ports"     = ["8001", "8002"]
  }
}

variable "tcp_lbs_altssh" {
  type = "map"

  default = {
    "names"                      = ["https"]
    "forwarding_port_ranges"     = ["443"]
    "health_check_ports"         = ["8003"]
    "health_check_request_paths" = ["/-/available-ssh"]
  }
}

variable "tcp_lbs_bastion" {
  type = "map"

  default = {
    "names"                  = ["ssh"]
    "forwarding_port_ranges" = ["22"]
    "health_check_ports"     = ["80"]
  }
}

#######################

variable "public_ports" {
  type = "map"

  default = {
    "api"         = []
    "bastion"     = [22]
    "blackbox"    = []
    "console"     = []
    "consul"      = []
    "deploy"      = []
    "runner"      = []
    "db"          = []
    "pgb"         = []
    "fe-lb"       = [22, 80, 443, 2222]
    "geodb"       = []
    "git"         = []
    "mailroom"    = []
    "pubsubbeat"  = []
    "redis"       = []
    "redis-cache" = []
    "registry"    = []
    "sidekiq"     = []
    "sd-exporter" = []
    "stor"        = []
    "web"         = []
    "monitoring"  = []
    "influxdb"    = []
  }
}

variable "environment" {
  default = "gstg"
}

variable "format_data_disk" {
  default = "true"
}

variable "project" {
  default = "gitlab-staging-1"
}

variable "region" {
  default = "us-east1"
}

variable "chef_provision" {
  type        = "map"
  description = "Configuration details for chef server"

  default = {
    bootstrap_bucket  = "gitlab-gstg-chef-bootstrap"
    bootstrap_key     = "gitlab-gstg-bootstrap-validation"
    bootstrap_keyring = "gitlab-gstg-bootstrap"

    server_url    = "https://chef.gitlab.com/organizations/gitlab/"
    user_name     = "gitlab-ci"
    user_key_path = ".chef.pem"
    version       = "12.19.36"
  }
}

variable "chef_version" {
  default = "12.19.36"
}

variable "monitoring_cert_link" {
  default = "projects/gitlab-staging-1/global/sslCertificates/wildcard-gstg-gitlab-net"
}

variable "data_disk_sizes" {
  type = "map"

  default = {
    "file"  = "2000"
    "share" = "1000"
    "pages" = "1000"
  }
}

variable "machine_types" {
  type = "map"

  default = {
    "api"                   = "n1-standard-4"
    "bastion"               = "g1-small"
    "blackbox"              = "n1-standard-1"
    "console"               = "n1-standard-1"
    "consul"                = "n1-standard-4"
    "deploy"                = "n1-standard-2"
    "runner"                = "n1-standard-2"
    "db"                    = "n1-standard-8"
    "fe-lb"                 = "n1-standard-4"
    "geodb"                 = "n1-standard-8"
    "git"                   = "n1-standard-8"
    "influxdb"              = "n1-standard-4"
    "pgb"                   = "n1-standard-4"
    "mailroom"              = "n1-standard-2"
    "monitoring"            = "n1-standard-4"
    "redis"                 = "n1-standard-8"
    "redis-cache"           = "n1-standard-4"
    "redis-cache-sentinel"  = "n1-standard-1"
    "registry"              = "n1-standard-2"
    "sd-exporter"           = "n1-standard-1"
    "sidekiq-asap"          = "n1-standard-8"
    "sidekiq-besteffort"    = "n1-standard-8"
    "sidekiq-elasticsearch" = "n1-standard-8"
    "sidekiq-pages"         = "n1-standard-8"
    "sidekiq-pipeline"      = "n1-standard-8"
    "sidekiq-pullmirror"    = "n1-standard-8"
    "sidekiq-realtime"      = "n1-standard-8"
    "sidekiq-traces"        = "n1-standard-8"

    "stor" = "n1-standard-4"
    "web"  = "n1-standard-8"
  }
}

variable "node_count" {
  type = "map"

  default = {
    "api"                   = 1
    "bastion"               = 1
    "blackbox"              = 1
    "console"               = 1
    "deploy"                = 1
    "runner"                = 1
    "consul"                = 3
    "db"                    = 4
    "fe-lb"                 = 2
    "fe-lb-pages"           = 2
    "fe-lb-altssh"          = 2
    "geodb"                 = 1
    "git"                   = 1
    "mailroom"              = 1
    "pages"                 = 1
    "pgb"                   = 3
    "redis"                 = 3
    "redis-cache"           = 3
    "redis-cache-sentinel"  = 3
    "registry"              = 1
    "sd-exporter"           = 1
    "share"                 = 1
    "sidekiq-asap"          = 1
    "sidekiq-besteffort"    = 3
    "sidekiq-elasticsearch" = 1
    "sidekiq-pages"         = 1
    "sidekiq-pipeline"      = 0
    "sidekiq-pullmirror"    = 1
    "sidekiq-realtime"      = 1
    "sidekiq-traces"        = 1
    "stor"                  = 2
    "web"                   = 1
    "prometheus"            = 2
    "prometheus-app"        = 2
    "alerts"                = 2
    "influxdb"              = 2
  }
}

variable "subnetworks" {
  type = "map"

  default = {
    "api"          = "10.224.12.0/24"
    "db"           = "10.224.6.0/24"
    "console"      = "10.224.21.0/24"
    "consul"       = "10.224.4.0/24"
    "fe-lb"        = "10.224.14.0/24"
    "fe-lb-pages"  = "10.224.18.0/24"
    "fe-lb-altssh" = "10.224.19.0/24"
    "git"          = "10.224.13.0/24"
    "redis"        = "10.224.7.0/24"
    "redis-cache"  = "10.224.8.0/24"
    "stor"         = "10.224.2.0/23"
    "web"          = "10.224.1.0/24"
    "db"           = "10.224.6.0/24"
    "pgb"          = "10.224.9.0/24"
    "sidekiq"      = "10.225.1.0/24"
    "registry"     = "10.224.10.0/24"
    "mailroom"     = "10.224.11.0/24"
    "deploy"       = "10.224.15.0/24"
    "runner"       = "10.224.16.0/24"
    "geodb"        = "10.224.17.0/24"
    "monitoring"   = "10.226.1.0/24"
    "pubsubbeat"   = "10.226.2.0/24"
    "bastion"      = "10.224.20.0/24"
    "influxdb"     = "10.226.3.0/24"

    ###############################
    # These will eventually (tm) be
    # moved to object storage

    "pages" = "10.224.32.0/24"
    "share" = "10.224.33.0/24"

    #############################
  }
}

variable "vpn_peer_address" {
  type    = "string"
  default = "52.177.223.41"
}

variable "vpn_dest_subnet" {
  type = "string"

  // 10.129.1.0/24 PostgresStaging
  default = "10.129.1.0/24"
}

variable "vpn_source_subnet" {
  type = "string"

  // 10.224.0.0/13 for all of GSTG
  // 10.224.6.0/24 for dbGSTG
  default = "10.224.0.0/13"
}

variable "vpn_shared_secret" {
  type = "string"
}

variable "service_account_email" {
  type = "string"

  default = "terraform@gitlab-staging-1.iam.gserviceaccount.com"
}

variable "gcs_service_account_email" {
  type    = "string"
  default = "gitlab-object-storage@gitlab-staging-1.iam.gserviceaccount.com"
}
