variable "environment" {
  default = "stg"
}

## Azure

variable "arm_subscription_id" {}
variable "arm_client_id" {}
variable "arm_client_secret" {}
variable "arm_tenant_id" {}

# We need these variables as part of the virtual machine creation.
# These will go away as soon as we switch to pre-baked server images.
# - Daniele

variable "first_user_username" {}
variable "first_user_password" {}

# These are the new variables to connect to the newly created instance, which
# replace the two above.

variable "ssh_user" {}
variable "ssh_private_key" {}
variable "ssh_public_key" {}

# This must be removed after porting all the modules to the private/public keys
variable "ssh_key" {}

variable "location" {
  default = "East US 2"
}

provider "azurerm" {
  subscription_id = "${var.arm_subscription_id}"
  client_id       = "${var.arm_client_id}"
  client_secret   = "${var.arm_client_secret}"
  tenant_id       = "${var.arm_tenant_id}"
}

## Chef
variable "chef_version" {
  default = "12.19.36"
}

variable "chef_repo_dir" {}

## AWS
provider "aws" {
  region = "us-east-1"
}

variable "gitlab_com_zone_id" {}
variable "gitlab_net_zone_id" {}

## State storage
terraform {
  backend "s3" {}
}

### Vnet

module "vnet" {
  source               = "vnet"
  location             = "${var.location}"
  virtual_network_cidr = "10.128.0.0/11"
}

### Subnets

module "subnet-external-lb" {
  source              = "subnets/external-lb"
  location            = "${var.location}"
  subnet_cidr         = "10.128.1.0/24"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-internal-lb" {
  source              = "subnets/internal-lb"
  location            = "${var.location}"
  subnet_cidr         = "10.128.2.0/24"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-postgres" {
  source              = "subnets/postgres"
  location            = "${var.location}"
  subnet_cidr         = "10.129.1.0/24"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-redis" {
  source              = "subnets/redis"
  location            = "${var.location}"
  subnet_cidr         = "10.129.2.0/24"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-elasticsearch" {
  source              = "subnets/elasticsearch"
  location            = "${var.location}"
  subnet_cidr         = "10.129.3.0/24"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-pgbouncer" {
  source              = "subnets/pgbouncer"
  location            = "${var.location}"
  subnet_cidr         = "10.129.4.0/24"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-consul" {
  source              = "subnets/consul"
  location            = "${var.location}"
  subnet_cidr         = "10.130.1.0/24"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-vault" {
  source              = "subnets/vault"
  location            = "${var.location}"
  subnet_cidr         = "10.130.2.0/24"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
  vault_clients       = "${module.vnet.virtual_network_cidr}"
}

module "subnet-deploy" {
  source              = "subnets/deploy"
  location            = "${var.location}"
  subnet_cidr         = "10.130.3.0/24"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-monitoring" {
  source              = "subnets/monitoring"
  location            = "${var.location}"
  subnet_cidr         = "10.131.1.0/24"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-log" {
  source              = "subnets/log"
  location            = "${var.location}"
  subnet_cidr         = "10.131.2.0/24"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-api" {
  source              = "subnets/api"
  location            = "${var.location}"
  subnet_cidr         = "10.132.2.0/23"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-git" {
  source              = "subnets/git"
  location            = "${var.location}"
  subnet_cidr         = "10.132.4.0/23"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-sidekiq" {
  source              = "subnets/sidekiq"
  location            = "${var.location}"
  subnet_cidr         = "10.132.6.0/23"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-web" {
  source              = "subnets/web"
  location            = "${var.location}"
  subnet_cidr         = "10.132.8.0/23"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-registry" {
  source              = "subnets/registry"
  location            = "${var.location}"
  subnet_cidr         = "10.132.10.0/23"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-api-internal" {
  source              = "subnets/api-internal"
  location            = "${var.location}"
  subnet_cidr         = "10.132.12.0/23"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-mailroom" {
  source              = "subnets/mailroom"
  location            = "${var.location}"
  subnet_cidr         = "10.132.14.0/23"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-storage" {
  source              = "subnets/storage"
  location            = "${var.location}"
  subnet_cidr         = "10.133.2.0/23"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

### Virtul Machines

module "virtual-machines-external-lb" {
  altssh_lb_count     = 1
  pages_lb_count      = 1
  source              = "virtual-machines/external-lb"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-external-lb.resource_group_name}"
  subnet_id           = "${module.subnet-external-lb.subnet_id}"
  first_user_username = "${var.first_user_username}"
  first_user_password = "${var.first_user_password}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "{\"syslog_client\": \"stg\", \"gitlab-staging-base\": \"stg\", \"gitlab-staging-external-lb\": \"_default\"}"
  gitlab_com_zone_id  = "${var.gitlab_com_zone_id}"
}

module "virtual-machines-internal-lb" {
  backend_lb_count    = 0
  source              = "../../modules/virtual-machines/internal-lb"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-internal-lb.resource_group_name}"
  subnet_id           = "${module.subnet-internal-lb.subnet_id}"
  instance_type       = "Standard_A1_v2"
  tier                = "lb"
  environment         = "${var.environment}"
  address_prefix      = "${module.subnet-internal-lb.address_prefix}"
  first_user_username = "${var.first_user_username}"
  first_user_password = "${var.first_user_password}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "{\"syslog_client\": \"stg\", \"gitlab-staging-base\": \"stg\", \"staging-base-lb\": \"backend\"}"
  gitlab_com_zone_id  = "${var.gitlab_com_zone_id}"
}

module "virtual-machines-postgres" {
  count               = 4
  source              = "virtual-machines/postgres"
  resource_group_name = "${module.subnet-postgres.resource_group_name}"
  subnet_id           = "${module.subnet-postgres.subnet_id}"
  instance_type       = "Standard_DS5_v2"
  tier                = "db"
  environment         = "${var.environment}"
  address_prefix      = "${module.subnet-postgres.address_prefix}"
  location            = "${var.location}"
  first_user_username = "${var.first_user_username}"
  first_user_password = "${var.first_user_password}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "{\"syslog_client\": \"stg\", \"gitlab-staging-base\": \"stg\", \"gitlab-monitor\": \"stg\", \"postgres-exporter\": \"stg\"}"
  gitlab_com_zone_id  = "${var.gitlab_com_zone_id}"
}

module "virtual-machines-redis" {
  source              = "virtual-machines/redis"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-redis.resource_group_name}"
  subnet_id           = "${module.subnet-redis.subnet_id}"
  first_user_username = "${var.first_user_username}"
  first_user_password = "${var.first_user_password}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "{\"syslog_client\": \"stg\", \"gitlab-staging-base\": \"stg\"}"
  gitlab_com_zone_id  = "${var.gitlab_com_zone_id}"
}

/* Commenting this out until the Redis split is fully supported in Omnibus
module "virtual-machines-redis-cache" {
  address_prefix         = "${module.subnet-redis.address_prefix}"
  chef_repo_dir          = "${var.chef_repo_dir}"
  chef_vaults            = "{\"syslog_client\": \"stg\", \"gitlab-staging-base\": \"stg\", \"gitlab_consul\": \"stg_client\"}"
  chef_version           = "${var.chef_version}"
  environment            = "${var.environment}"
  gitlab_com_zone_id     = "${var.gitlab_com_zone_id}"
  ip_skew                = 10
  location               = "${var.location}"
  persistence            = "cache"
  redis_count            = 1
  redis_instance_type    = "Standard_DS2_v2"
  resource_group_name    = "${module.subnet-redis.resource_group_name}"
  sentinel_count         = 3
  sentinel_instance_type = "Standard_A1_v2"
  source                 = "../../modules/virtual-machines/redis"
  ssh_private_key        = "${var.ssh_private_key}"
  ssh_public_key         = "${var.ssh_public_key}"
  ssh_user               = "${var.ssh_user}"
  subnet_id              = "${module.subnet-redis.subnet_id}"
  tier                   = "db"
}
*/

module "virtual-machines-elasticsearch" {
  count               = 0
  source              = "virtual-machines/elasticsearch"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-elasticsearch.resource_group_name}"
  subnet_id           = "${module.subnet-elasticsearch.subnet_id}"
  first_user_username = "${var.first_user_username}"
  first_user_password = "${var.first_user_password}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "{\"syslog_client\": \"stg\", \"gitlab-staging-base\": \"stg\"}"
  gitlab_com_zone_id  = "${var.gitlab_com_zone_id}"
}

module "virtual-machines-pgbouncer" {
  address_prefix      = "${module.subnet-pgbouncer.address_prefix}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "{\"syslog_client\": \"stg\", \"gitlab-staging-base\": \"stg\", \"gitlab-monitor\": \"stg\", \"postgres-exporter\": \"stg\"}"
  chef_version        = "${var.chef_version}"
  count               = 1
  environment         = "${var.environment}"
  gitlab_com_zone_id  = "${var.gitlab_com_zone_id}"
  instance_type       = "Standard_A1_v2"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-pgbouncer.resource_group_name}"
  source              = "../../modules/virtual-machines/pgbouncer"
  ssh_private_key     = "${var.ssh_private_key}"
  ssh_public_key      = "${var.ssh_public_key}"
  ssh_user            = "${var.ssh_user}"
  subnet_id           = "${module.subnet-pgbouncer.subnet_id}"
  tier                = "db"
}

module "virtual-machines-deploy" {
  source              = "virtual-machines/deploy"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-deploy.resource_group_name}"
  subnet_id           = "${module.subnet-deploy.subnet_id}"
  first_user_username = "${var.first_user_username}"
  first_user_password = "${var.first_user_password}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "{\"syslog_client\": \"stg\", \"gitlab-staging-base\": \"stg\"}"
  gitlab_com_zone_id  = "${var.gitlab_com_zone_id}"
}

module "virtual-machines-consul" {
  count               = 3
  instance_type       = "Standard_A2_v2"
  source              = "../../modules/virtual-machines/consul"
  resource_group_name = "${module.subnet-consul.resource_group_name}"
  subnet_id           = "${module.subnet-consul.subnet_id}"
  tier                = "inf"
  environment         = "${var.environment}"
  address_prefix      = "${module.subnet-consul.address_prefix}"
  location            = "${var.location}"
  first_user_username = "${var.first_user_username}"
  first_user_password = "${var.first_user_password}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "{\"syslog_client\": \"stg\", \"gitlab-cluster-base\": \"_default\"}"
  gitlab_net_zone_id  = "${var.gitlab_net_zone_id}"
}

module "virtual-machines-api" {
  address_prefix      = "${module.subnet-api.address_prefix}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "{\"syslog_client\": \"stg\", \"gitlab-staging-base\": \"stg\", \"gitlab_consul\": \"stg_client\"}"
  chef_version        = "${var.chef_version}"
  count               = 1
  environment         = "${var.environment}"
  gitlab_com_zone_id  = "${var.gitlab_com_zone_id}"
  instance_type       = "Standard_F4s"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-api.resource_group_name}"
  source              = "../../modules/virtual-machines/api"
  ssh_private_key     = "${var.ssh_private_key}"
  ssh_public_key      = "${var.ssh_public_key}"
  ssh_user            = "${var.ssh_user}"
  subnet_id           = "${module.subnet-api.subnet_id}"
  tier                = "sv"
}

module "virtual-machines-git" {
  address_prefix      = "${module.subnet-git.address_prefix}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "{\"syslog_client\": \"stg\", \"gitlab-staging-base\": \"stg\", \"gitlab_consul\": \"stg_client\"}"
  chef_version        = "${var.chef_version}"
  count               = 1
  environment         = "${var.environment}"
  gitlab_com_zone_id  = "${var.gitlab_com_zone_id}"
  instance_type       = "Standard_F4s"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-git.resource_group_name}"
  source              = "../../modules/virtual-machines/git"
  ssh_private_key     = "${var.ssh_private_key}"
  ssh_public_key      = "${var.ssh_public_key}"
  ssh_user            = "${var.ssh_user}"
  subnet_id           = "${module.subnet-git.subnet_id}"
  tier                = "sv"
}

module "virtual-machines-sidekiq" {
  address_prefix                      = "${module.subnet-sidekiq.address_prefix}"
  chef_repo_dir                       = "${var.chef_repo_dir}"
  chef_vaults                         = "{\"syslog_client\": \"stg\", \"gitlab-staging-base\": \"stg\", \"gitlab_consul\": \"stg_client\"}"
  chef_version                        = "${var.chef_version}"
  environment                         = "${var.environment}"
  first_user_password                 = "${var.first_user_password}"
  first_user_username                 = "${var.first_user_username}"
  gitlab_com_zone_id                  = "${var.gitlab_com_zone_id}"
  location                            = "${var.location}"
  resource_group_name                 = "${module.subnet-sidekiq.resource_group_name}"
  sidekiq_asap_count                  = 1
  sidekiq_asap_instance_type          = "Standard_D3_v2"
  sidekiq_besteffort_count            = 1
  sidekiq_besteffort_instance_type    = "Standard_A4_v2"
  sidekiq_elasticsearch_count         = 0
  sidekiq_elasticsearch_instance_type = "Standard_A4_v2"
  sidekiq_pages_count                 = 0
  sidekiq_pages_instance_type         = "Standard_A4_v2"
  sidekiq_pipeline_count              = 1
  sidekiq_pipeline_instance_type      = "Standard_A4_v2"
  sidekiq_pullmirror_count            = 0
  sidekiq_pullmirror_instance_type    = "Standard_A4_v2"
  sidekiq_realtime_count              = 1
  sidekiq_realtime_instance_type      = "Standard_D3_v2"
  sidekiq_traces_count                = 0
  sidekiq_traces_instance_type        = "Standard_A4_v2"
  source                              = "../../modules/virtual-machines/sidekiq"
  ssh_private_key                     = "${var.ssh_private_key}"
  ssh_public_key                      = "${var.ssh_public_key}"
  ssh_user                            = "${var.ssh_user}"
  subnet_id                           = "${module.subnet-sidekiq.subnet_id}"
  tier                                = "sv"
}

module "virtual-machines-web" {
  address_prefix      = "${module.subnet-web.address_prefix}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "{\"syslog_client\": \"stg\", \"gitlab-staging-base\": \"stg\", \"gitlab_consul\": \"stg_client\"}"
  chef_version        = "${var.chef_version}"
  count               = 2
  environment         = "${var.environment}"
  gitlab_com_zone_id  = "${var.gitlab_com_zone_id}"
  instance_type       = "Standard_F4s"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-web.resource_group_name}"
  source              = "../../modules/virtual-machines/web"
  ssh_private_key     = "${var.ssh_private_key}"
  ssh_public_key      = "${var.ssh_public_key}"
  ssh_user            = "${var.ssh_user}"
  subnet_id           = "${module.subnet-web.subnet_id}"
  tier                = "sv"
}

module "virtual-machines-registry" {
  count               = 2
  source              = "../../modules/virtual-machines/registry"
  resource_group_name = "${module.subnet-registry.resource_group_name}"
  subnet_id           = "${module.subnet-registry.subnet_id}"
  instance_type       = "Standard_A1_v2"
  tier                = "sv"
  environment         = "${var.environment}"
  address_prefix      = "${module.subnet-registry.address_prefix}"
  location            = "${var.location}"
  first_user_username = "${var.first_user_username}"
  first_user_password = "${var.first_user_password}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "{\"syslog_client\": \"stg\", \"gitlab-staging-base\": \"stg\"}"
  gitlab_com_zone_id  = "${var.gitlab_com_zone_id}"
}

module "virtual-machines-mailroom" {
  address_prefix      = "${module.subnet-mailroom.address_prefix}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "{\"syslog_client\": \"stg\", \"gitlab-staging-base\": \"stg\", \"gitlab_consul\": \"stg_client\"}"
  chef_version        = "${var.chef_version}"
  count               = 1
  environment         = "${var.environment}"
  gitlab_com_zone_id  = "${var.gitlab_com_zone_id}"
  instance_type       = "Standard_A1_v2"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-mailroom.resource_group_name}"
  source              = "../../modules/virtual-machines/mailroom"
  ssh_private_key     = "${var.ssh_private_key}"
  ssh_public_key      = "${var.ssh_public_key}"
  ssh_user            = "${var.ssh_user}"
  subnet_id           = "${module.subnet-mailroom.subnet_id}"
  tier                = "sv"
}

module "virtual-machines-api-internal" {
  count               = 0
  source              = "../../modules/virtual-machines/api-internal"
  resource_group_name = "${module.subnet-api-internal.resource_group_name}"
  subnet_id           = "${module.subnet-api-internal.subnet_id}"
  instance_type       = "Standard_A2_v2"
  tier                = "sv"
  environment         = "${var.environment}"
  address_prefix      = "${module.subnet-api-internal.address_prefix}"
  location            = "${var.location}"
  first_user_username = "${var.first_user_username}"
  first_user_password = "${var.first_user_password}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "{\"syslog_client\": \"stg\", \"gitlab-staging-base\": \"stg\"}"
  gitlab_com_zone_id  = "${var.gitlab_com_zone_id}"
}

module "virtual-machines-vault" {
  address_prefix      = "${module.subnet-vault.address_prefix}"
  count               = 1
  environment         = "${var.environment}"
  gitlab_net_zone_id  = "${var.gitlab_net_zone_id}"
  instance_type       = "Standard_A0_v2"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-vault.resource_group_name}"
  security_group_id   = "${module.subnet-vault.security_group_id}"
  source              = "../../modules/virtual-machines/vault"
  subnet_id           = "${module.subnet-vault.subnet_id}"
  tier                = "inf"
}
