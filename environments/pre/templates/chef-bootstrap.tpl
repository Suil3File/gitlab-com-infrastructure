set -exu

cd ${chef_repo_dir}

bundle exec knife bootstrap ${first_user_username}@${ip_address} -P ${first_user_password} --no-host-key-verify --sudo --node-name ${hostname} --bootstrap-version "12.19.36" -r 'role[gitlab]' -j {\"azure\":{\"ipaddress\":\"${ip_address}\"}} -y

for i in ${chef_vaults}
do
  vault="`echo $i | cut -d : -f 1`"
  if [[ $i == *':'* ]]; then
    item="`echo $i | cut -d : -f 2`"
  elif [[ $i == "syslog_client" ]]; then
    item="_default"
  else
    item="${chef_vault_env}"
  fi
  bundle exec rake 'add_node_secrets[${hostname},'$vault','$item']'
done

bundle exec knife node from file nodes/${hostname}.json
