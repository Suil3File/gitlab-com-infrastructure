resource "azurerm_availability_set" "RedisPre" {
  name                         = "RedisPre"
  location                     = "${var.location}"
  managed                      = true
  platform_update_domain_count = 20
  platform_fault_domain_count  = 3
  resource_group_name          = "${var.resource_group_name}"
}

resource "azurerm_managed_disk" "redis-01-datadisk-0" {
  name                 = "redis-01-pre-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "20"
}

resource "azurerm_public_ip" "redis-01" {
  name                         = "redis-01-pre-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "redis-01-pre"
}

resource "azurerm_network_interface" "redis-01" {
  name                    = "redis-01-pre"
  internal_dns_name_label = "redis-01-pre"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "redis-01-pre"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.161.2.101"
    public_ip_address_id          = "${azurerm_public_ip.redis-01.id}"
  }
}

resource "aws_route53_record" "redis-01" {
  zone_id = "${var.gitlab_com_zone_id}"
  name    = "redis-01.pre.gitlab.com"
  type    = "CNAME"
  ttl     = "300"
  records = ["${azurerm_public_ip.redis-01.fqdn}."]
}

data "template_file" "chef-bootstrap-redis-01" {
  template = "${file("${path.root}/templates/chef-bootstrap.tpl")}"

  vars {
    ip_address          = "${azurerm_public_ip.redis-01.ip_address}"
    hostname            = "redis-01.pre.gitlab.com"
    chef_repo_dir       = "${var.chef_repo_dir}"
    first_user_username = "${var.first_user_username}"
    first_user_password = "${var.first_user_password}"
    chef_vaults         = "${var.chef_vaults}"
    chef_vault_env      = "${var.chef_vault_env}"
  }
}

resource "azurerm_virtual_machine" "redis-01" {
  name                          = "redis-01.pre.gitlab.com"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  availability_set_id           = "${azurerm_availability_set.RedisPre.id}"
  network_interface_ids         = ["${azurerm_network_interface.redis-01.id}"]
  primary_network_interface_id  = "${azurerm_network_interface.redis-01.id}"
  vm_size                       = "Standard_DS11_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-redis-01-pre"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.redis-01-datadisk-0.name}"
    managed_disk_id = "${azurerm_managed_disk.redis-01-datadisk-0.id}"
    disk_size_gb    = "${azurerm_managed_disk.redis-01-datadisk-0.disk_size_gb}"
    create_option   = "Attach"
    lun             = 0
    caching         = "ReadWrite"
  }

  os_profile {
    computer_name  = "redis-01.pre.gitlab.com"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  provisioner "local-exec" {
    command = "${data.template_file.chef-bootstrap-redis-01.rendered}"
  }

  provisioner "remote-exec" {
    inline = ["nohup bash -c 'sudo chef-client &'"]

    connection {
      type     = "ssh"
      host     = "${azurerm_public_ip.redis-01.ip_address}"
      user     = "${var.first_user_username}"
      password = "${var.first_user_password}"
      timeout  = "10s"
    }
  }
}
