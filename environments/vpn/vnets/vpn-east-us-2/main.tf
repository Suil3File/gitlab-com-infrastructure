variable "location" {
  description = "The location"
}

variable "address_space" {
  type        = "list"
  description = "A list of CIDRs that for the address space for this network"
}

resource "azurerm_resource_group" "VPN-East-US-2" {
  name     = "VPN-East-US-2"
  location = "${var.location}"
}

resource "azurerm_virtual_network" "VPN-East-US-2" {
  name                = "VPN-East-US-2"
  address_space       = "${var.address_space}"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.VPN-East-US-2.name}"
}

output "id" {
  value = "${azurerm_virtual_network.VPN-East-US-2.id}"
}

output "name" {
  value = "${azurerm_virtual_network.VPN-East-US-2.name}"
}

output "resource_group_name" {
  value = "${azurerm_resource_group.VPN-East-US-2.name}"
}
