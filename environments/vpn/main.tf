variable "arm_subscription_id" {}
variable "arm_client_id" {}
variable "arm_client_secret" {}
variable "arm_tenant_id" {}
variable "chef_repo_dir" {}
variable "gitlab_com_zone_id" {}
variable "gitlab_net_zone_id" {}

# We need these variables as part of the virtual machine creation.
# These will go away as soon as we switch to pre-baked server images.
# - Daniele
variable "first_user_username" {}

variable "first_user_password" {}

variable "location" {
  default = "East US 2"
}

provider "azurerm" {
  subscription_id = "${var.arm_subscription_id}"
  client_id       = "${var.arm_client_id}"
  client_secret   = "${var.arm_client_secret}"
  tenant_id       = "${var.arm_tenant_id}"
}

provider "aws" {
  region = "us-east-1"
}

terraform {
  backend "s3" {}
}

### Vnets

module "vpn-east-us-2" {
  source        = "vnets/vpn-east-us-2"
  location      = "${var.location}"
  address_space = ["10.254.4.0/23"]
}

### Subnets

module "subnet-vpn-east-us-2" {
  source              = "subnets/vpn-east-us-2"
  location            = "${var.location}"
  subnet_cidr         = "10.254.4.0/23"
  vnet_name           = "${module.vpn-east-us-2.name}"
  vnet_resource_group = "${module.vpn-east-us-2.resource_group_name}"
}

### Virtual Machines

module "virtual-machines-vpn-east-us-2" {
  count               = 2
  source              = "virtual-machines/vpn-east-us-2"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-vpn-east-us-2.resource_group_name}"
  subnet_id           = "${module.subnet-vpn-east-us-2.subnet_id}"
  first_user_username = "${var.first_user_username}"
  first_user_password = "${var.first_user_password}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "syslog_client gitlab-cluster-base"
  gitlab_net_zone_id  = "${var.gitlab_net_zone_id}"
}
