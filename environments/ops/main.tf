## State storage
terraform {
  backend "s3" {}
}

## AWS
provider "aws" {
  region = "us-east-1"
}

variable "gitlab_com_zone_id" {}
variable "gitlab_net_zone_id" {}

## Google

provider "google" {
  version = "~> 1.8.0"
  project = "${var.project}"
  region  = "${var.region}"
}

##################################
#
#  Network
#
#################################

module "network" {
  source      = "../../modules/google/vpc"
  project     = "${var.project}"
  environment = "${var.environment}"
}

##################################
#
#  Network Peering
#
#################################

resource "google_compute_network_peering" "peering_gprd" {
  name         = "peering-gprd"
  network      = "${var.network_ops}"
  peer_network = "${var.network_gprd}"
}

resource "google_compute_network_peering" "peering_gstg" {
  name         = "peering-gstg"
  network      = "${var.network_ops}"
  peer_network = "${var.network_gstg}"
}

##################################
#
#  Log Proxy
#
#################################

module "proxy-iap" {
  environment          = "${var.environment}"
  source               = "../../modules/google/web-iap"
  name                 = "proxy"
  project              = "${var.project}"
  region               = "${var.region}"
  gitlab_zone_id       = "${var.gitlab_net_zone_id}"
  cert_link            = "${var.log_gitlab_net_cert_link}"
  backend_service_link = "${module.proxy.google_compute_backend_service_iap_self_link}"
  web_ip_fqdn          = "log.gitlab.net"
  service_ports        = ["443", "80", "9090"]
}

module "proxy" {
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-infra-proxy]\""
  dns_zone_name         = "${var.dns_zone_name}"
  enable_iap            = true
  environment           = "${var.environment}"
  health_check          = "http"
  ip_cidr_range         = "${var.subnetworks["proxy"]}"
  machine_type          = "${var.machine_types["proxy"]}"
  name                  = "proxy"
  node_count            = 1
  oauth2_client_id      = "${var.oauth2_client_id_log_proxy}"
  oauth2_client_secret  = "${var.oauth2_client_secret_log_proxy}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["proxy"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = "9090"
  source                = "../../modules/google/generic-sv-with-group"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

#######################################################
#
# Load balancer and VM for dashboards.gitlab.net
#
#######################################################

module "dashboards-iap" {
  environment          = "${var.environment}"
  source               = "../../modules/google/web-iap"
  name                 = "dashboards"
  project              = "${var.project}"
  region               = "${var.region}"
  gitlab_zone_id       = "${var.gitlab_net_zone_id}"
  cert_link            = "${var.dashboards_gitlab_net_cert_link}"
  backend_service_link = "${module.dashboards.google_compute_backend_service_self_link}"
  web_ip_fqdn          = "dashboards.gitlab.net"
  service_ports        = ["80", "443"]
}

module "dashboards" {
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-infra-dashboards]\""
  data_disk_size        = 100
  data_disk_type        = "pd-ssd"
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "http"
  health_check_port     = 80
  ip_cidr_range         = "${var.subnetworks["dashboards"]}"
  machine_type          = "${var.machine_types["dashboards"]}"
  name                  = "dashboards"
  node_count            = 1
  oauth2_client_id      = "${var.oauth2_client_id_dashboards}"
  oauth2_client_secret  = "${var.oauth2_client_secret_dashboards}"
  persistent_disk_path  = "/var/lib/grafana"
  project               = "${var.project}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_path          = "/"
  service_port          = 80
  source                = "../../modules/google/monitoring-with-count"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

module "monitor" {
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-infra-public-dashboards]\""
  data_disk_size        = 100
  data_disk_type        = "pd-standard"
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "http"
  health_check_port     = 80
  ip_cidr_range         = "${var.subnetworks["monitor"]}"
  machine_type          = "${var.machine_types["monitor"]}"
  name                  = "monitor"
  node_count            = 1
  oauth2_client_id      = "${var.oauth2_client_id_dashboards}"
  oauth2_client_secret  = "${var.oauth2_client_secret_dashboards}"
  persistent_disk_path  = "/var/lib/grafana"
  project               = "${var.project}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_path          = "/"
  service_port          = 80
  source                = "../../modules/google/monitoring-with-count"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

#######################################################
#
# VM for ops.gitlab.net
#
#######################################################

resource "aws_route53_record" "default" {
  zone_id = "${var.gitlab_net_zone_id}"
  name    = "ops.gitlab.net"
  type    = "A"
  ttl     = "300"
  records = ["${module.gitlab-ops.instance_public_ips[0]}"]
}

module "gitlab-ops" {
  backend_protocol      = "HTTPS"
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-infra-gitlab]\""
  data_disk_size        = 500
  data_disk_type        = "pd-ssd"
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "http"
  health_check_port     = 8887
  ip_cidr_range         = "${var.subnetworks["gitlab-ops"]}"
  machine_type          = "${var.machine_types["gitlab-ops"]}"
  name                  = "gitlab"
  node_count            = 1
  oauth2_client_id      = "${var.oauth2_client_id_gitlab_ops}"
  oauth2_client_secret  = "${var.oauth2_client_secret_gitlab_ops}"
  persistent_disk_path  = "/var/opt/gitlab"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["gitlab-ops"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_path          = "/-/liveness"
  service_port          = 443
  source                = "../../modules/google/monitoring-with-count"
  tier                  = "inf"
  use_external_ip       = true
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

###############################################
#
# Load balancer and VM for the ops bastion
#
###############################################

module "gcp-tcp-lb-bastion" {
  environment            = "${var.environment}"
  forwarding_port_ranges = "${var.tcp_lbs_bastion["forwarding_port_ranges"]}"
  fqdns                  = "${var.lb_fqdns_bastion}"
  gitlab_zone_id         = "${var.gitlab_com_zone_id}"
  health_check_ports     = "${var.tcp_lbs_bastion["health_check_ports"]}"
  instances              = ["${module.bastion.instances_self_link}"]
  lb_count               = "${length(var.tcp_lbs_bastion["names"])}"
  name                   = "gcp-tcp-lb-bastion"
  names                  = "${var.tcp_lbs_bastion["names"]}"
  project                = "${var.project}"
  region                 = "${var.region}"
  session_affinity       = "CLIENT_IP"
  source                 = "../../modules/google/tcp-lb"
  targets                = ["bastion"]
}

module "bastion" {
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-bastion]\""
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  ip_cidr_range         = "${var.subnetworks["bastion"]}"
  machine_type          = "${var.machine_types["bastion"]}"
  name                  = "bastion"
  node_count            = "${var.node_count["bastion"]}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["bastion"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 22
  source                = "../../modules/google/generic-sv-with-group"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  Google storage buckets
#
##################################

module "storage" {
  environment                       = "${var.environment}"
  versioning                        = "${var.versioning}"
  artifact_age                      = "${var.artifact_age}"
  lfs_object_age                    = "${var.lfs_object_age}"
  upload_age                        = "${var.upload_age}"
  storage_log_age                   = "${var.storage_log_age}"
  storage_class                     = "${var.storage_class}"
  gcs_service_account_email         = "${var.gcs_service_account_email}"
  gcs_storage_analytics_group_email = "${var.gcs_storage_analytics_group_email}"
  source                            = "../../modules/google/storage-buckets"
}

##################################
#
#  GitLab Billing bucket
#
##################################

resource "google_storage_bucket" "gitlab-billing" {
  name = "gitlab-billing"

  versioning = {
    enabled = "false"
  }

  storage_class = "NEARLINE"

  labels = {
    tfmanaged = "yes"
  }
}

resource "google_storage_bucket_iam_binding" "billing-viewer-binding" {
  bucket = "gitlab-billing"
  role   = "roles/storage.objectViewer"

  members = [
    "serviceAccount:import@gitlab-analysis.iam.gserviceaccount.com",
  ]
}

resource "google_storage_bucket_iam_binding" "billing-legacy-bucket-binding" {
  bucket = "gitlab-billing"
  role   = "roles/storage.legacyBucketReader"

  members = [
    "serviceAccount:import@gitlab-analysis.iam.gserviceaccount.com",
    "projectViewer:gitlab-ops",
  ]
}

resource "google_storage_bucket_iam_binding" "billing-legacy-object-binding" {
  bucket = "gitlab-billing"
  role   = "roles/storage.legacyObjectReader"

  members = [
    "serviceAccount:import@gitlab-analysis.iam.gserviceaccount.com",
  ]
}
