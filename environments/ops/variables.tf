variable "project" {
  default = "gitlab-ops"
}

variable "region" {
  default = "us-east1"
}

variable "environment" {
  default = "ops"
}

variable "dns_zone_name" {
  default = "gitlab.net"
}

variable "oauth2_client_id_log_proxy" {}
variable "oauth2_client_secret_log_proxy" {}
variable "oauth2_client_id_dashboards" {}
variable "oauth2_client_secret_dashboards" {}
variable "oauth2_client_id_gitlab_ops" {}
variable "oauth2_client_secret_gitlab_ops" {}

variable "machine_types" {
  type = "map"

  default = {
    "log-proxy"  = "n1-standard-1"
    "proxy"      = "n1-standard-1"
    "bastion"    = "n1-standard-1"
    "dashboards" = "n1-standard-4"
    "monitor"    = "n1-standard-8"
    "gitlab-ops" = "n1-standard-16"
  }
}

variable "service_account_email" {
  type = "string"

  default = "terraform@gitlab-ops.iam.gserviceaccount.com"
}

# The ops network is allocated
# 10.250.0.0/16

variable "subnetworks" {
  type = "map"

  default = {
    "logging"    = "10.250.1.0/24"
    "bastion"    = "10.250.2.0/24"
    "dashboards" = "10.250.3.0/24"
    "gitlab-ops" = "10.250.4.0/24"
    "proxy"      = "10.250.5.0/24"
    "monitor"    = "10.250.6.0/24"
  }
}

variable "public_ports" {
  type = "map"

  default = {
    "log-proxy"  = []
    "proxy"      = []
    "bastion"    = [22]
    "dashboards" = []
    "gitlab-ops" = [443, 80, 22, 5005]
  }
}

variable "node_count" {
  type = "map"

  default = {
    "bastion"    = 1
    "dashboards" = 1
    "gitlab-ops" = 1
  }
}

variable "chef_provision" {
  type        = "map"
  description = "Configuration details for chef server"

  default = {
    bootstrap_bucket  = "gitlab-ops-chef-bootstrap"
    bootstrap_key     = "gitlab-ops-bootstrap-validation"
    bootstrap_keyring = "gitlab-ops-bootstrap"

    server_url    = "https://chef.gitlab.com/organizations/gitlab/"
    user_name     = "gitlab-ci"
    user_key_path = ".chef.pem"
    version       = "12.19.36"
  }
}

variable "chef_version" {
  default = "12.19.36"
}

variable "lb_fqdns_bastion" {
  type    = "list"
  default = ["lb-bastion.ops.gitlab.com"]
}

variable "network_ops" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-ops/global/networks/ops"
}

variable "network_gprd" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-production/global/networks/gprd"
}

variable "network_gstg" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-staging-1/global/networks/gstg"
}

variable "tcp_lbs_bastion" {
  type = "map"

  default = {
    "names"                  = ["ssh"]
    "forwarding_port_ranges" = ["22"]
    "health_check_ports"     = ["80"]
  }
}

variable "log_gitlab_net_cert_link" {
  default = "projects/gitlab-ops/global/sslCertificates/log-gitlab-net"
}

variable "ops_gitlab_net_cert_link" {
  default = "projects/gitlab-ops/global/sslCertificates/ops-gitlab-net"
}

variable "dashboards_gitlab_net_cert_link" {
  default = "projects/gitlab-ops/global/sslCertificates/dashboards-gitlab-net"
}

variable "gcs_service_account_email" {
  type    = "string"
  default = "gitlab-object-storage@gitlab-ops.iam.gserviceaccount.com"
}
