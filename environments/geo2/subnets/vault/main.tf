variable "location" {}
variable "vnet_name" {}
variable "vnet_resource_group" {}
variable "subnet_cidr" {}

variable "vault_clients" {
  description = "CIDR, permitted to access Vault (tcp/8200)"
}

resource "azurerm_resource_group" "VaultGeo2" {
  name     = "VaultGeo2"
  location = "${var.location}"
}

resource "azurerm_network_security_group" "VaultGeo2" {
  name                = "VaultGeo2"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.VaultGeo2.name}"
}

resource "azurerm_network_security_rule" "vault" {
  name                        = "vault"
  description                 = "All of pre (${var.vault_clients}) to pre-vault (${azurerm_subnet.VaultGeo2.address_prefix}), tcp/8200"
  priority                    = 150
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "${var.vault_clients}"
  destination_port_range      = "8200"
  destination_address_prefix  = "${azurerm_subnet.VaultGeo2.address_prefix}"
  resource_group_name         = "${azurerm_resource_group.VaultGeo2.name}"
  network_security_group_name = "${azurerm_network_security_group.VaultGeo2.name}"
}

resource "azurerm_network_security_rule" "prometheus" {
  name                        = "prometheus"
  description                 = "Prometheus (10.4.1.0/24) to pre-vault (${azurerm_subnet.VaultGeo2.address_prefix}), tcp/9100"
  priority                    = 151
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "10.4.1.0/24"
  destination_port_range      = "9100"
  destination_address_prefix  = "${azurerm_subnet.VaultGeo2.address_prefix}"
  resource_group_name         = "${azurerm_resource_group.VaultGeo2.name}"
  network_security_group_name = "${azurerm_network_security_group.VaultGeo2.name}"
}

resource "azurerm_subnet" "VaultGeo2" {
  name                      = "VaultGeo2"
  resource_group_name       = "${var.vnet_resource_group}"
  virtual_network_name      = "${var.vnet_name}"
  address_prefix            = "${var.subnet_cidr}"
  network_security_group_id = "${azurerm_network_security_group.VaultGeo2.id}"
}

output "address_prefix" {
  value = "${azurerm_subnet.VaultGeo2.address_prefix}"
}

output "subnet_id" {
  value = "${azurerm_subnet.VaultGeo2.id}"
}

output "resource_group_name" {
  value = "VaultGeo2"
}

output "resource_group_id" {
  value = "${azurerm_resource_group.VaultGeo2.id}"
}

output "security_group_id" {
  value = "${azurerm_network_security_group.VaultGeo2.id}"
}
