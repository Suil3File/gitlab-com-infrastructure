variable "location" {}
variable "vnet_name" {}
variable "vnet_resource_group" {}
variable "subnet_cidr" {}

resource "azurerm_resource_group" "MonitoringGeo2" {
  name     = "MonitoringGeo2"
  location = "${var.location}"
}

resource "azurerm_network_security_group" "MonitoringGeo2" {
  name                = "MonitoringGeo2"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.MonitoringGeo2.name}"
}

resource "azurerm_network_security_rule" "http" {
  name                        = "http"
  priority                    = 140
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "Internet"
  destination_port_range      = "80"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.MonitoringGeo2.name}"
  network_security_group_name = "${azurerm_network_security_group.MonitoringGeo2.name}"
}

resource "azurerm_network_security_rule" "https" {
  name                        = "https"
  priority                    = 145
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "Internet"
  destination_port_range      = "443"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.MonitoringGeo2.name}"
  network_security_group_name = "${azurerm_network_security_group.MonitoringGeo2.name}"
}

resource "azurerm_network_security_rule" "ssh-from-vpn1-ext" {
  name                        = "ssh-from-vpn1-ext"
  priority                    = 146
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "52.177.194.133"
  destination_port_range      = "22"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.MonitoringGeo2.name}"
  network_security_group_name = "${azurerm_network_security_group.MonitoringGeo2.name}"
}

resource "azurerm_network_security_rule" "ssh-from-vpn2-ext" {
  name                        = "ssh-from-vpn2-ext"
  priority                    = 147
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "52.177.192.239"
  destination_port_range      = "22"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.MonitoringGeo2.name}"
  network_security_group_name = "${azurerm_network_security_group.MonitoringGeo2.name}"
}

resource "azurerm_network_security_rule" "ssh-from-internal" {
  name                        = "ssh-from-internal"
  priority                    = 148
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "10.0.0.0/8"
  destination_port_range      = "22"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.MonitoringGeo2.name}"
  network_security_group_name = "${azurerm_network_security_group.MonitoringGeo2.name}"
}

resource "azurerm_network_security_rule" "ssh-from-vpn" {
  name                        = "ssh-from-vpn"
  priority                    = 149
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "10.254.4.0/23"
  destination_port_range      = "22"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.MonitoringGeo2.name}"
  network_security_group_name = "${azurerm_network_security_group.MonitoringGeo2.name}"
}

resource "azurerm_network_security_rule" "ssh" {
  name                        = "ssh"
  priority                    = 150
  direction                   = "Inbound"
  access                      = "Deny"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "Internet"
  destination_port_range      = "22"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.MonitoringGeo2.name}"
  network_security_group_name = "${azurerm_network_security_group.MonitoringGeo2.name}"
}

resource "azurerm_network_security_rule" "checkmk" {
  name                        = "checkmk"
  priority                    = 152
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "52.28.130.79/32"
  destination_port_range      = "6556"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.MonitoringGeo2.name}"
  network_security_group_name = "${azurerm_network_security_group.MonitoringGeo2.name}"
}

resource "azurerm_subnet" "MonitoringGeo2" {
  name                      = "MonitoringGeo2"
  resource_group_name       = "${var.vnet_resource_group}"
  virtual_network_name      = "${var.vnet_name}"
  address_prefix            = "${var.subnet_cidr}"
  network_security_group_id = "${azurerm_network_security_group.MonitoringGeo2.id}"
}

output "subnet_id" {
  value = "${azurerm_subnet.MonitoringGeo2.id}"
}

output "address_prefix" {
  value = "${azurerm_subnet.MonitoringGeo2.address_prefix}"
}

output "resource_group_name" {
  value = "MonitoringGeo2"
}

output "resource_group_id" {
  value = "${azurerm_resource_group.MonitoringGeo2.id}"
}
