variable "location" {
  description = "The location"
}

variable "resource_group_name" {
  description = "The name of the resource group"
}

variable "security_group_id" {}
variable "subnet_id" {}
variable "first_user_username" {}
variable "first_user_password" {}

resource "azurerm_availability_set" "MonitoringGeo2" {
  name                         = "MonitoringGeo2"
  location                     = "${var.location}"
  platform_update_domain_count = 20
  platform_fault_domain_count  = 3
  resource_group_name          = "${var.resource_group_name}"
}

output "availability_set_id" {
  value = "${azurerm_availability_set.MonitoringGeo2.id}"
}
