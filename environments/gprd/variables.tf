variable "oauth2_client_id_monitoring" {}
variable "oauth2_client_secret_monitoring" {}

variable "gitlab_net_zone_id" {}
variable "gitlab_com_zone_id" {}
variable "gitlab_io_zone_id" {}

variable "default_kernel_version" {
  default = "4.10.0-1009"
}

variable "monitoring_hosts" {
  type = "map"

  default = {
    "names" = ["alerts", "prometheus", "prometheus-app"]
    "ports" = [9093, 9090, 9090]
  }
}

#### GCP load balancing

# The top level domain record for the GitLab deployment.
# For production this should be set to "gitlab.com"

variable "lb_fqdns" {
  type    = "list"
  default = ["gprd.gitlab.com", "registry.gitlab.com"]
}

##########
variable "lb_fqdns_altssh" {
  type    = "list"
  default = ["altssh.gprd.gitlab.com"]
}

variable "lb_fqdns_pages" {
  type    = "list"
  default = ["*.pages.gprd.gitlab.io"]
}

variable "lb_fqdns_bastion" {
  type    = "list"
  default = ["lb-bastion.gprd.gitlab.com"]
}

variable "lb_fqdns_internal" {
  type    = "list"
  default = ["int.gprd.gitlab.net"]
}

variable "lb_fqdns_internal_pgbouncer" {
  type    = "list"
  default = ["pgbouncer.int.gprd.gitlab.net"]
}

#
# For every name there must be a corresponding
# forwarding port range and health check port
#

variable "tcp_lbs" {
  type = "map"

  default = {
    "names"                  = ["http", "https", "ssh"]
    "forwarding_port_ranges" = ["80", "443", "22"]
    "health_check_ports"     = ["8001", "8002", "8003"]
  }
}

variable "tcp_lbs_internal" {
  type = "map"

  default = {
    "names"                  = ["http-internal", "https-internal", "ssh-internal"]
    "forwarding_port_ranges" = ["80", "443", "22"]
    "health_check_ports"     = ["8001", "8002", "8003"]
  }
}

variable "tcp_lbs_pages" {
  type = "map"

  default = {
    "names"                  = ["http", "https"]
    "forwarding_port_ranges" = ["80", "443"]
    "health_check_ports"     = ["8001", "8002"]
  }
}

variable "tcp_lbs_altssh" {
  type = "map"

  default = {
    "names"                      = ["https"]
    "forwarding_port_ranges"     = ["443"]
    "health_check_ports"         = ["8003"]
    "health_check_request_paths" = ["/-/available-ssh"]
  }
}

variable "tcp_lbs_bastion" {
  type = "map"

  default = {
    "names"                  = ["ssh"]
    "forwarding_port_ranges" = ["22"]
    "health_check_ports"     = ["80"]
  }
}

##################
# Network Peering
##################

variable "network_ops" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-ops/global/networks/ops"
}

variable "network_env" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-production/global/networks/gprd"
}

#######################
# pubsubbeat config
#######################

variable "pubsubbeats" {
  type = "map"

  default = {
    "names"         = ["gitaly", "haproxy", "pages", "postgres", "production", "system", "workhorse", "geo", "sidekiq", "api", "nginx", "gitlab-shell", "shell", "rails", "unstructured", "unicorn", "application", "redis"]
    "machine_types" = ["n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-1", "n1-standard-1", "n1-standard-1"]
  }
}

######################

variable "base_chef_run_list" {
  default = "\"role[gitlab]\",\"recipe[gitlab_users::default]\",\"recipe[gitlab_sudo::default]\",\"recipe[gitlab-server::bashrc]\""
}

variable "empty_chef_run_list" {
  default = "\"\""
}

variable "dns_zone_name" {
  default = "gitlab.com"
}

variable "run_lists" {
  type = "map"

  default = {
    "prometheus"  = "\"role[gitlab]\",\"recipe[gitlab_users::default]\",\"recipe[gitlab_sudo::default]\",\"recipe[gitlab-server::bashrc]\""
    "performance" = "\"role[gitlab]\",\"recipe[gitlab_users::default]\",\"recipe[gitlab_sudo::default]\",\"recipe[gitlab-server::bashrc]\""
  }
}

variable "public_ports" {
  type = "map"

  default = {
    "api"         = []
    "bastion"     = [22]
    "blackbox"    = []
    "consul"      = []
    "console"     = []
    "deploy"      = []
    "runner"      = []
    "db"          = []
    "pgb"         = []
    "fe-lb"       = [22, 80, 443, 2222]
    "geodb"       = []
    "git"         = []
    "mailroom"    = []
    "pubsubbeat"  = []
    "redis"       = []
    "redis-cache" = []
    "registry"    = []
    "sidekiq"     = []
    "sd-exporter" = []
    "stor"        = []
    "web"         = []
    "monitoring"  = []
    "influxdb"    = []
  }
}

variable "environment" {
  default = "gprd"
}

variable "format_data_disk" {
  default = "true"
}

variable "project" {
  default = "gitlab-production"
}

variable "region" {
  default = "us-east1"
}

variable "chef_provision" {
  type        = "map"
  description = "Configuration details for chef server"

  default = {
    bootstrap_bucket  = "gitlab-gprd-chef-bootstrap"
    bootstrap_key     = "gitlab-gprd-bootstrap-validation"
    bootstrap_keyring = "gitlab-gprd-bootstrap"

    server_url    = "https://chef.gitlab.com/organizations/gitlab/"
    user_name     = "gitlab-ci"
    user_key_path = ".chef.pem"
    version       = "12.19.36"
  }
}

variable "chef_version" {
  default = "12.19.36"
}

variable "monitoring_cert_link" {
  default = "projects/gitlab-production/global/sslCertificates/wildcard-gprd-gitlab-net"
}

variable "data_disk_sizes" {
  type = "map"

  default = {
    "file"  = "16000"
    "share" = "16000"
    "pages" = "16000"
  }
}

variable "machine_types" {
  type = "map"

  default = {
    "api"                   = "n1-standard-8"
    "bastion"               = "g1-small"
    "blackbox"              = "n1-standard-1"
    "consul"                = "n1-standard-4"
    "db"                    = "n1-highmem-64"
    "console"               = "n1-standard-1"
    "deploy"                = "n1-standard-2"
    "fe-lb"                 = "n1-standard-4"
    "geodb"                 = "n1-highmem-32"
    "git"                   = "n1-standard-16"
    "influxdb"              = "n1-standard-8"
    "mailroom"              = "n1-standard-4"
    "monitoring"            = "n1-standard-8"
    "pgb"                   = "n1-standard-4"
    "redis"                 = "n1-highmem-16"
    "redis-cache"           = "n1-highmem-16"
    "redis-cache-sentinel"  = "n1-standard-1"
    "registry"              = "n1-standard-2"
    "runner"                = "n1-standard-2"
    "sd-exporter"           = "n1-standard-1"
    "sidekiq-asap"          = "n1-standard-8"
    "sidekiq-besteffort"    = "n1-standard-8"
    "sidekiq-elasticsearch" = "n1-standard-8"
    "sidekiq-pages"         = "n1-standard-8"
    "sidekiq-pipeline"      = "n1-standard-8"
    "sidekiq-pullmirror"    = "n1-standard-8"
    "sidekiq-realtime"      = "n1-standard-8"
    "sidekiq-traces"        = "n1-standard-8"
    "stor"                  = "n1-standard-4"
    "web"                   = "n1-standard-16"
  }
}

variable "node_count" {
  type = "map"

  default = {
    "api"                   = 14
    "bastion"               = 3
    "blackbox"              = 1
    "console"               = 1
    "consul"                = 3
    "db"                    = 4
    "deploy"                = 1
    "fe-lb"                 = 2
    "fe-lb-altssh"          = 2
    "fe-lb-pages"           = 2
    "geodb"                 = 1
    "git"                   = 12
    "mailroom"              = 2
    "pages"                 = 1
    "pgb"                   = 3
    "redis"                 = 3
    "redis-cache"           = 3
    "redis-cache-sentinel"  = 3
    "registry"              = 2
    "runner"                = 1
    "share"                 = 1
    "sd-exporter"           = 1
    "sidekiq-asap"          = 5
    "sidekiq-besteffort"    = 6
    "sidekiq-elasticsearch" = 0
    "sidekiq-pages"         = 6
    "sidekiq-pipeline"      = 3
    "sidekiq-pullmirror"    = 5
    "sidekiq-realtime"      = 4
    "sidekiq-traces"        = 2
    "stor"                  = 20
    "web"                   = 14
    "alerts"                = 2
    "prometheus"            = 2
    "prometheus-app"        = 2
    "influxdb"              = 2
  }
}

variable "subnetworks" {
  type = "map"

  default = {
    "fe-lb"        = "10.216.1.0/24"
    "fe-lb-pages"  = "10.216.2.0/24"
    "fe-lb-altssh" = "10.216.3.0/24"
    "bastion"      = "10.216.4.0/24"
    "db"           = "10.217.1.0/24"
    "redis"        = "10.217.2.0/24"
    "pgb"          = "10.217.4.0/24"
    "redis-cache"  = "10.217.5.0/24"
    "geodb"        = "10.217.6.0/24"
    "consul"       = "10.218.1.0/24"
    "deploy"       = "10.218.3.0/24"
    "runner"       = "10.218.4.0/24"
    "console"      = "10.218.5.0/24"
    "monitoring"   = "10.219.1.0/24"
    "pubsubbeat"   = "10.219.2.0/24"
    "registry"     = "10.220.10.0/23"
    "mailroom"     = "10.220.14.0/23"
    "api"          = "10.220.2.0/23"
    "git"          = "10.220.4.0/23"
    "sidekiq"      = "10.220.6.0/23"
    "web"          = "10.220.8.0/23"
    "stor"         = "10.221.2.0/23"
    "influxdb"     = "10.219.3.0/24"

    ###############################
    # These will eventually (tm) be
    # moved to object storage

    "pages" = "10.221.6.0/24"
    "share" = "10.221.7.0/24"

    #############################
  }
}

variable "vpn_peer_address" {
  type    = "string"
  default = "40.70.42.69"
}

variable "vpn_dest_subnet" {
  type = "string"

  // 10.66.1.0/24 PostgresProd
  // 10.67.3.0/24 deploy prod, for testing
  default = "10.66.1.0/24"
}

variable "vpn_source_subnet" {
  type = "string"

  // 10.216.0.0/13 for all of GitLabGeoPrd
  // 10.217.1.0/24 for DBGPrd
  default = "10.216.0.0/13"
}

variable "vpn_shared_secret" {
  type = "string"
}

variable "service_account_email" {
  type = "string"

  default = "terraform@gitlab-production.iam.gserviceaccount.com"
}

variable "gcs_service_account_email" {
  type    = "string"
  default = "gitlab-object-storage-prd@gitlab-production.iam.gserviceaccount.com"
}
